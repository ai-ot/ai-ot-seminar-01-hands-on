/**
 * Gulp, Browserify関連の外部ライブラリです
 */
import gulp         from 'gulp'
import gulpif       from 'gulp-if'
import autoprefixer from 'gulp-autoprefixer'
import concat       from 'gulp-concat'
import minify       from 'gulp-clean-css'
import plumber      from 'gulp-plumber'
import sass         from 'gulp-sass'
import sourcemaps   from 'gulp-sourcemaps'
import uglify       from 'gulp-uglify'
import browserify   from 'browserify'
import babelify     from 'babelify'
import streamqueue  from 'streamqueue'
import source       from 'vinyl-source-stream'
import buffer       from 'vinyl-buffer'
import browserSync  from 'browser-sync'
import { protractor } from 'gulp-protractor'

/**
 * 開発に関係するサーバーなどを読み込みます
 */
import createMockServer   from './server/createMockServer'
import createStaticServer from './server/createStaticServer'

// 環境変数からNODE環境を読み込みます
const ENV = process.env.NODE_ENV || 'development'
// ルートディレクトリです。
const BASE = './'
// 本アプリのエントリポイントです。
const jsEntry = BASE + 'src/main.jsx'
// CSS関連のファイルです。
const styles = [BASE + 'styles/*.scss']
// 外部のスタイルシートです。
const STYLE_CSS_EXTERNAL = [
  BASE + 'node_modules/rc-time-picker/assets/index.css'
]
// React関連及びその他の純粋なJavScriptライブラリのファイルです。
const scripts = [
  BASE + 'src/*.js',
  BASE + 'src/*.jsx',
  BASE + 'src/actions/*.js',
  BASE + 'src/lib/*.js',
  BASE + 'src/components/*.jsx',
  BASE + 'src/mixins/*.jsx'
]
// バンドル後のCSSファイル名です。
const STYLE_CSS = 'style.css'
// バンドル後のJavaScriptファイル名です。
const BUNDLE_JS = 'bundle.js'
// シングルページアプリケーションのhtmlファイル名です。
const INDEX_HTML = 'index.html'
// 開発に使うポートです。以下のポートを他で使っている場合、別の値に変えて下さい。
const PORTS = [2999, 3000]

/**
 * CSSのビルドを行います。
 * @type {Task}
 */
gulp.task('css', () => {

  streamqueue(
    { objectMode: true },
    // project css
    gulp.src(styles)
      .pipe(plumber())
      .pipe(gulpif(ENV !== 'production', sourcemaps.init()))
      .pipe(sass()),

      // 外部CSS
      gulp.src(STYLE_CSS_EXTERNAL)
  )
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat(STYLE_CSS))
    .pipe(gulpif(ENV === 'production', minify({ keepSpecialComments: 1 })))
    .pipe(gulpif(ENV !== 'production', sourcemaps.write('./maps/')))
    .pipe(gulp.dest(BASE))
})

/**
 * JavaScriptのビルドを行います。
 * コードはUglifyされます。
 * @type {Task}
 */
gulp.task('js', () => {

  browserify({ entries: jsEntry })
    .transform([babelify])
    .bundle()
    .pipe(source(BUNDLE_JS))
    .pipe(buffer())
    .pipe(gulpif(ENV !== 'production', sourcemaps.init()))
    .pipe(gulpif(ENV === 'production', uglify({ preserveComments: true })))
    .pipe(gulpif(ENV !== 'production', sourcemaps.write('./maps/')))
    .pipe(gulp.dest(BASE))
})

/**
 * CSS及びJavaScriptのビルドを行います。
 * @type {Task}
 */
gulp.task('build', ['css', 'js'])

/**
 * 開発用サーバーを立ち上げます。
 * @type {Task}
 */
gulp.task('serve', ['build'], () => {

  createMockServer({ port: PORTS[0] })
    .then(() => {
      browserSync.init({
        port: PORTS[1],
        server: { baseDir: BASE }
      })
      gulp.watch(styles,  ['css'])
      gulp.watch(scripts, ['js'])
      gulp.watch([
        BASE + INDEX_HTML,
        BASE + STYLE_CSS,
        BASE + BUNDLE_JS
      ])
        .on('change', browserSync.reload)
    })
})

/**
 * e2eテストを実行します。
 * @type {Task}
 */
gulp.task('test:e2e', () => {

  Promise.all([
    createMockServer({ port: PORTS[0], quiet: false }),
    createStaticServer(PORTS[1])
  ])
  .then(() => {
    gulp.src(['./test/e2e/*.js'])
      .pipe(protractor({
        configFile: './protractor.conf.js',
        args: ['--baseUrl', `http://127.0.0.1:${PORTS[1]}`]
      }))
      .on('error', e => { throw e })
  })
})
