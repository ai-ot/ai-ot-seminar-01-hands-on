import fs from 'fs'

if (process.argv.length < 3) {
  process.stdout.write('引数が必要です。')
  process.exit(1)
}

const slug = process.argv[2]
const COMP_SRC  = __dirname + '/fixture/Component.jsx.template'
const TEST_SRC  = __dirname + '/fixture/Component.unit.jsx.template'
const COMP_DEST = __dirname + '/../src/components/' + slug + '.jsx'
const TEST_DEST = __dirname + '/../test/unit/components/' + slug + '.unit.jsx'

/**
 * コンポーネントのテンプレートをコピーします
 * @param  {String} from path from
 * @param  {String} into path into
 * @param  {String} slug Component slug init with Uppercase.
 * @return {Promse}      [description]
 */
const copyTemplate = (from, into, slug) => new Promise(resolved => {
  fs.readFile(into, err => {
    if (err) {
      // file not exist
      fs.readFile(from, (err, temp) => {
        const data = temp.toString()
          .replace(/{{SLUG}}/g, slug)
          .replace(/{{slug}}/g, slug.toLowerCase())
        fs.writeFile(into, data, () => {
          process.stdout.write(`${into}が生成されました。\n`)
          resolved()
        })
      })
    } else {
      // file exists
      process.stdout.write(`${into}はすでに存在しています。`)
      resolved()
    }
  })
})

Promise.all([
  copyTemplate(COMP_SRC, COMP_DEST, slug),
  copyTemplate(TEST_SRC, TEST_DEST, slug)
]).catch(err => process.stdout.write(err))
