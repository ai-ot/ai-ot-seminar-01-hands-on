#!usr/bin/env bash
# スクリーンショットの差分検証を行います
# imagemagickに依存します

set -eu

# 引数チェック
if [ $# -ne 2 ]; then
  echo "2つのcommitを指定してください" 1>&2
  exit 1
fi
COMMIT_A=$1
COMMIT_B=$2

# duplicate module components
if [ -d './.tmp' ]; then rm -rf './.tmp'; fi
mkdir ./.tmp
mkdir ./.tmp/project
mkdir ./.tmp/artifacts
mkdir ./.tmp/artifacts/a
mkdir ./.tmp/artifacts/b
mkdir ./.tmp/artifacts/diff

echo 'プロジェクトを複製しています..'
rsync -a ./ ./.tmp/project --exclude './tmp'
cd './.tmp/project'

echo "ロールバックしています..$COMMIT_A"
git stash
git checkout $COMMIT_A
npm install       >/dev/null 2>&1
npm run build:dev >/dev/null 2>&1

echo 'スクリーンショットを作成しています..'
npm run screenshot >/dev/null 2>&1
cp -r ./screenshots/* ../artifacts/a

sleep 1s

echo "ロールバックしています..$COMMIT_B"
git stash
git checkout $COMMIT_B
npm install       >/dev/null 2>&1
npm run build:dev >/dev/null 2>&1

echo 'スクリーンショットを作成しています..'
npm run screenshot >/dev/null 2>&1
cp -r ./screenshots/* ../artifacts/b

cd '../artifacts'

# 差分画像を生成
# ファイル名は`./screenshot.jsに依存`
composite -compose difference ./a/tb.png ./b/tb.png ./diff/tb.png
# 差分の定量化
DIFF_MEAN=$(identify -format "%[mean]" ./diff/tb.png)
# 枠と文字をつける
convert ./a/tb.png    -bordercolor "#777" -border 2x2 -gravity North -splice 0x60 -pointsize 40 -annotate 0x0 "Before" ./a/tb.png
convert ./b/tb.png    -bordercolor "#777" -border 2x2 -gravity North -splice 0x60 -pointsize 40 -annotate 0x0 "After"  ./b/tb.png
convert ./diff/tb.png -bordercolor "#777" -border 2x2 -gravity North -splice 0x60 -pointsize 40 -annotate 0x0 "Diff($DIFF_MEAN)" ./diff/tb.png

# 連結
if [ ! -d ../../screenshots/diff ]; then mkdir ../../screenshots/diff ; fi
echo "{\"tb\":$DIFF_MEAN}" > ../../screenshots/diff/mean.json
convert +append ./a/tb.png ./b/tb.png ./diff/tb.png ../../screenshots/diff/tb.png

# clean up
cd '../..'
rm -rf ./tmp
exit 0
