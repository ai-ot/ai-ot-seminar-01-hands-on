#!usr/bin/env node
// CircleCI環境下で、差分を比較するべきコミットIDを取得します

// $CIRCLE_COMPARE_URLで /(commitid)..(commitid)を取得
// ない場合は$CIRCLE_BRANCHとmasterを比較

// - [OK] HEADを伸ばすコミットプッシュ
// - [OK] マージコミットのプッシュ
// - [NG] 新しいブランチを生やすコミットプッシュ

const URL = process.env.CIRCLE_COMPARE_URL
const BRANCH = process.env.CIRCLE_BRANCH
const matched = (typeof URL === 'string' ? URL : '').match(/[0-9,a-f]{40}/g)

const [commit_b, commit_a] = (matched && matched.length === 2) ?
    matched :
    BRANCH === 'master' ?
      ['master~', 'master'] :
      ['master', BRANCH]

process.stdout.write([commit_a, commit_b].join(' '))
