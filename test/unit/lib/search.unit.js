import {
  getListIndex,
  getPostitIndex,
} from '../../../src/lib/search'
import 'should'

describe('list IDからのlistIndexの検索', () => {

  describe('dockに入っていない時', () => {

    let state

    beforeEach(() => {
      state = {
        lists: [{}, {}, { id: 'the-list' }]
      }
    })

    it('見つけたlistのindex値を返す', () => {
      getListIndex(state, 'the-list').listIndex.should.equal(2)
    })

    it('dock自体を指していないことを示す', () => {
      getListIndex(state, 'the-list').isDock.should.be.False()
    })

    it('見つからなかったらindex値は-1', () => {
      getListIndex(state, 'list-not-found').listIndex.should.equal(-1)
    })

    it('見つからない場合でもdockを指していないことを示す', () => {
      getListIndex(state, 'list-not-found').isDock.should.be.False()
    })
  })

  describe('dockを指している時', () => {

    let state

    beforeEach(() => {
      state = { dock: { id: 'the-dock' } }
    })

    it('indexはネガティブ値(-1)を返す', () => {
      getListIndex(state, 'the-dock').listIndex.should.equal(-1)
    })

    it('dock自体を指していることを示す', () => {
      getListIndex(state, 'the-dock').isDock.should.be.True()
    })
  })
})

describe('postit IDからのpostitIndex、listIndexの検索', () => {

  describe('dockにない場合', () => {

    let state

    beforeEach(() => {
      state = {
        lists: [
          {},
          {
            id: 'list1',
            postits: [
              { id: 'postit1' },
              { id: 'postit2' }
            ]
          }
        ]
      }
    })

    it('リストをインデックス値を返す', () => {
      getPostitIndex(state, 'postit1').listIndex.should.equal(1)
    })

    it('カードのインデックス値を返す', () => {
      getPostitIndex(state, 'postit1').postitIndex.should.equal(0)
    })

    it('Dockに入っていないことが判定できる', () => {
      getPostitIndex(state, 'postit1').isDocked.should.be.False()
    })

    it('見つからない時は、リストのインデックス値が-1', () => {
      getPostitIndex(state, 'postit-not-defined').listIndex.should.equal(-1)
    })

    it('見つからない時は、カードのインデックス値が-1', () => {
      getPostitIndex(state, 'postit-not-defined').postitIndex.should.equal(-1)
    })

    it('見つからない時も、Dockに入っていないことが判定できる', () => {
      getPostitIndex(state, 'postit-not-defined').isDocked.should.be.False()
    })
  })

  describe('dockにない場合', () => {

    it('単一のリストでもlistIDを検査できる', () => {
      const oldState = { lists: [{ postits: [{ id: 'aaapostit1' }, { id: 'aaapostit2' }] }] }
      getPostitIndex(oldState, 'aaapostit1').listIndex.should.equal(0)
    })

    it('単一のリストでもpostitIDを検査できる', () => {
      const oldState = { lists: [{ postits: [{ id: 'aaapostit1' }, { id: 'aaapostit2' }] }] }
      getPostitIndex(oldState, 'aaapostit2').postitIndex.should.equal(1)
    })
  })


  describe('dockにある場合', () => {

    let state

    beforeEach(() => {
      state = {
        dock: {
          postits: [
              { id: 'postit3' },
              { id: 'postit4' }
          ]
        },
        lists: [
          {},
          {
            id: 'list1',
            postits: [
              { id: 'postit1' },
              { id: 'postit2' }
            ]
          }
        ]
      }
    })

    it('リストをインデックス値を返さない', () => {
      getPostitIndex(state, 'postit3').listIndex.should.equal(-1)
    })

    it('カードのインデックス値を返す', () => {
      getPostitIndex(state, 'postit3').postitIndex.should.equal(0)
    })

    it('Dockに入っていることが判定できる', () => {
      getPostitIndex(state, 'postit3').isDocked.should.be.True()
    })
    it('見つからない時は、Dockに入っていないと判定される', () => {
      getPostitIndex(state, 'postit-not-defined').isDocked.should.be.False()
    })
  })
})
