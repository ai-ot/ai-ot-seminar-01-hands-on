import 'should'
import { JSON2state, state2JSON } from '../../../src/lib/stateAdaptor'

const json = {
  title: 'some title',
  lists: [
    {
      startAt: 1483591763584,
      leftAt: 1483591763584,
      id: 1,
      title: 'some list',
      postits: [
        {
          id: 2,
          title: 'some postit',
          content: 'some postit content'
        }
      ]
    }
  ],
  dock: {
    id: 'the-dock',
    title: 'dock',
    postits: [
      {
        id: 3,
        title: 'some postit',
        content: 'some postit content'
      }
    ]
  }
}

const state = {
  title: { editting: false, value: 'some title' },
  lists: [
    {
      startAt: 1483591763584,
      leftAt: 1483591763584,
      id: 1,
      title: { editting: false, value: 'some list' },
      postits: [
        {
          id: 2,
          title: { editting: false, value: 'some postit' },
          content: { editting: false, value: 'some postit content' }
        }
      ]
    }
  ],
  dock: {
    id: 'the-dock',
    title: { editting: false, value: 'dock' },
    postits: [
      {
        id: 3,
        title: { editting: false, value: 'some postit', },
        content: { editting: false, value: 'some postit content' }
      }
    ]
  }
}

describe('stateAdaptorのテスト', () => {

  it('読み込んだデータ->state', () => {
    JSON2state(json).should.deepEqual(state)
  })

  it('state->書き込むデータ', () => {
    state2JSON(state).should.deepEqual(json)
  })
})
