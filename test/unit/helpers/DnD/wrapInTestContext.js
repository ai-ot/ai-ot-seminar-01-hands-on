/**
 * DnD component test helper
 */

import React, { Component } from 'react'
import { DragDropContext } from 'react-dnd'
import TestBackend from 'react-dnd-test-backend'


/**
 * Wraps a component into a DragDropContext that uses the TestBackend.
 * https://gaearon.github.io/react-dnd/docs-testing.html
 * @param  {Component} DecoratedComponent [description]
 * @return {Component}              [description]
 */
export default function wrapInTestContext(DecoratedComponent) {
  return DragDropContext(TestBackend)(
    class TestContextContainer extends Component {

      /**
       * constructor
       * @return {ReactDOM} [ReactDOM]
       */
      render() {
        return <DecoratedComponent {...this.props} />
      }
    }
  )
}
