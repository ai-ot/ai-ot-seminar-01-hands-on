import jsdom from 'jsdom'

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>')

const _document  = global.document
const _window    = global.window
const _navigator = global.navigator

global.document  = doc
global.window    = doc.defaultView
global.navigator = { 'userAgent': 'iPhone' }

/**
 * recover global DOM object
 * @return {Function} [description]
 */
export const done = () => {
  global.document  = _document
  global.window    = _window
  global.navigator = _navigator
}
