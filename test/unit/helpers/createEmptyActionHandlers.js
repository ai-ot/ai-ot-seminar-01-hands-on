import { _allActions as actions } from '../../../src/redux'

const emptyActions = {}

Object.keys(actions).map(key => {
  emptyActions[key] = () => { /* empty functions */ }
})

export default () => emptyActions
