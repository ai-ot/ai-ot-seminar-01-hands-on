/**
 * テスト補助環境
 */
import TestUtils                 from 'react-addons-test-utils'
import createEmptyActionHandlers from '../helpers/createEmptyActionHandlers'
import 'should'

/**
 * React本体
 */
import React from 'react'

/**
 * テスト対象
 */
import Header from '../../../src/components/Header.jsx'

describe('Headerコンポーネントのテスト', () => {

  let root

  beforeEach(() => {
    const actionHandlers = createEmptyActionHandlers()
    root = TestUtils.renderIntoDocument(
      <Header
        id="the-header"
        title={ { value: 'the-header' } }
        actionHandlers={ actionHandlers } />
    )
  })

  it('headerクラスを持つ要素がレンダリングされる', () => {
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'header')
    elements.length.should.equal(1)
  })
})
