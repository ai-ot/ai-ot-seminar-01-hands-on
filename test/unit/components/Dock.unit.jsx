/**
 * テスト補助環境
 */
import done                      from '../helpers/initializeDOM'
import wrapInTestContext         from '../helpers/DnD/wrapInTestContext'
import createEmptyActionHandlers from '../helpers/createEmptyActionHandlers'
import TestUtils                 from 'react-addons-test-utils'
import                                'should'

/**
 * React本体
 */
import React from 'react'

/**
 * テスト対象
 */
import _Dock from '../../../src/components/Dock.jsx'

describe('Dockコンポーネントのテスト', () => {

  // Render with the test context that uses the test backend
  const Dock = wrapInTestContext(_Dock)

  describe('空の場合', () => {

    const root = TestUtils.renderIntoDocument(
      <Dock
        id='1'
        title={ { value: 'Dockのタイトル' } }
        actionHandlers={ createEmptyActionHandlers() } />
    )

    it('`dock`クラスをもつ要素が1つ作成される', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'dock')
      elements.length.should.equal(1)
    })

    it('`title`クラスをもつ要素が1つ作成される', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'title')
      elements.length.should.equal(1)
    })

    it('titleがレンダリングされる', () => {
      const elements = TestUtils
        .scryRenderedDOMComponentsWithClass(root, 'title')
        .map(component => component.textContent)
      elements[0].should.equal('Dockのタイトル')
    })

    it('`postit-empty`クラスを持つ要素が1つ作成される', () => {
      // TODO: 本来ならshallowレンダリングしてPostitEmptyコンポーネントが得られることをテストするべきだ
      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit-empty')
      element.length.should.equal(1)
    })
  })

  describe('変なタイトルを渡した場合', () => {

    [{ value: '' }, undefined].map(title => {

      it(`${title}を渡すとデフォルトの文字がレンダリングされる`, () => {

        const root = TestUtils.renderIntoDocument(
          <Dock
            id='1'
            title={ title }
            actionHandlers={ createEmptyActionHandlers() } />
        )

        const elements = TestUtils
          .scryRenderedDOMComponentsWithClass(root, 'title')
          .map(component => component.textContent)
        elements[0].should.equal('(NO TITLE)')
      })
    })
  })


  describe('postit要素を含む場合', () => {

    const postits = [{
      id: 'postit1',
      title: { value: '1枚目のカード' },
      content: { value: '1枚目のカードのコンテント' }
    }, {
      id: 'postit2',
      title: { value: '2枚目のカード' },
      content: { value: '2枚目のカードのコンテント' }
    }]
    const root = TestUtils.renderIntoDocument(
      <Dock
        id='1'
        title={ { value: 'Dockのタイトル' } }
        postits={ postits }
        actionHandlers={ createEmptyActionHandlers() } />
    )

    it('`postit`クラスをもつ要素が2つ作成される', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit')
      elements.length.should.equal(2)
    })

    it('`postit-empty`クラスを持つ要素が作成されない', () => {
      // TODO: 本来ならshallowレンダリングしてPostitEmptyコンポーネントが得られることをテストするべきだ
      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit-empty')
      element.length.should.equal(0)
    })
  })
})
