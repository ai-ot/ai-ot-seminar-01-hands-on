/**
 * Test with React DnD
 * https://gaearon.github.io/react-dnd/docs-testing.html
 */
import done from '../helpers/initializeDOM'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import 'should'

import TextEditable from '../../../src/components/TextEditable.jsx'

describe('TextEditableコンポーネントのテスト', () => {

  describe('編集開始のテスト', () => {

    let root = null
    let editting = false

    beforeEach(() => {

      root = TestUtils.renderIntoDocument(
        <TextEditable
          id='1'
          text={ { value: 'カードのタイトル' } }
          actionHandlers={ {
            startEdit: () => () => { editting = true }
          } } />
      )
    })

    it('テキストボックスはない', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithTag(root, 'input')
      elements.length.should.equal(0)
    })

    it('クリックするとstartEdit関数で得られる関数が呼ばれる', () => {
      editting.should.be.False()
      TestUtils.Simulate.click(
        TestUtils.scryRenderedDOMComponentsWithClass(root, 'click-to-input')[0]
      )
      editting.should.be.True()
    })
  })

  describe('編集終了のテスト', () => {

    let root = null
    let editting = true

    beforeEach(() => {
      root = TestUtils.renderIntoDocument(
        <TextEditable
          id='1'
          text={ { value: 'カードのタイトル', editting } }
          actionHandlers={ {
            startEdit: () => { /* モックの関数 */ },
            endEdit: () => () => { editting = false }
          } } />
      )
    })

    it('テキストボックスがある', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithTag(root, 'input')
      elements.length.should.equal(1)
    })

    it('アンフォーカスするとendEdit関数で得られる関数が呼ばれる', () => {
      const elements = TestUtils.scryRenderedDOMComponentsWithTag(root, 'input')
      TestUtils.Simulate.focus(elements[0])
      editting.should.be.True()
      TestUtils.Simulate.blur(elements[0])
      editting.should.be.False()
    })

  })
})
