/**
 * Test with React DnD
 * https://gaearon.github.io/react-dnd/docs-testing.html
 */
import done                      from '../helpers/initializeDOM'
import wrapInTestContext         from '../helpers/DnD/wrapInTestContext'
import createEmptyActionHandlers from '../helpers/createEmptyActionHandlers'
import TestUtils                 from 'react-addons-test-utils'
import 'should'

import React from 'react'

import _Board from '../../../src/components/Board.jsx'

describe('Boardコンポーネントのテスト', () => {

  // Render with the test context that uses the test backend
  const Board = wrapInTestContext(_Board)

  const root = TestUtils.renderIntoDocument(
    <Board
      id='1'
      title={ { value: 'カードのタイトル' } }
      content={ { value: 'カードのコンテンツ' } }
      dock={ { id: 'the-dock' } }
      actionHandlers={ createEmptyActionHandlers() } />
  )

  // Obtain a reference to the backend
  const backend = root.getManager().getBackend()

  it('`board`クラスを持つ要素が1つ作成される', () => {
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'board')
    elements.length.should.equal(1)
  })

  it('`title`クラスを持つ要素が2つ作成される', () => {
    // TODO: ここで言う2つとは、boardのタイトルとdockのタイトルのこと
    // board-titleクラスをレンダリングするようにした方が良さそう
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'title')
    elements.length.should.equal(2)
  })

  it('`header`クラスを持つ要素が1つ作成される', () => {
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'header')
    elements.length.should.equal(1)
  })

  it('`dock`クラスを持つ要素が1つ作成される', () => {
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'dock')
    elements.length.should.equal(1)
  })

  it('`footer`クラスを持つ要素が1つ作成される', () => {
    const elements = TestUtils.scryRenderedDOMComponentsWithClass(root, 'footer')
    elements.length.should.equal(1)
  })

  // // Find the drag source ID and use it to simulate the dragging operation
  // const box = TestUtils.findRenderedComponentWithType(root, Board)
  // backend.simulateBeginDrag([box.getHandlerId()])
  //
  // // Verify that the div changed its opacity
  // div = TestUtils.findRenderedDOMComponentWithTag(root, 'div')
  // expect(div.props.style.opacity).toEqual(0.4)

  // See other backend.simulate* methods for more!

})
