/**
 * Test with React DnD
 * https://gaearon.github.io/react-dnd/docs-testing.html
 */

 /**
 * テスト補助環境
 */
import done                       from  '../helpers/initializeDOM'
import wrapInTestContext          from  '../helpers/DnD/wrapInTestContext'
import createEmptyActionHandlers  from  '../helpers/createEmptyActionHandlers'
import TestUtils                  from  'react-addons-test-utils'
import { shallow, mount, render } from 'enzyme'
import { expect, should }         from 'chai'
/**
 * React本体
 */
import React from 'react'
/**
 * テスト対象
 */
import _Postit from '../../../src/components/Postit.jsx'

describe('Postitコンポーネントのテスト', () => {

  describe('Postitコンポーネント単体へのテスト', () => {

    let Postit
    let root
    let backend
    let swapped
    let confirmed

    beforeEach(() => {

      Postit = null
      root = null
      backend = null
      swapped = false
      confirmed = false
      // Render with the test context that uses the test backend
      Postit = wrapInTestContext(_Postit)

      const actionHandlers = createEmptyActionHandlers()
      actionHandlers.swapPostit = () => () => swapped = true
      actionHandlers.confirmPostit = () => () => confirmed = true
      root = TestUtils.renderIntoDocument(
        <Postit
          id='1'
          title={ { value: 'カードのタイトル' } }
          content={ { value: 'カードのコンテンツ' } }
          actionHandlers={ actionHandlers } />
      )

      // Obtain a reference to the backend
      backend = root.getManager().getBackend()
    })

    it('`postit`クラスを持つ要素が1つ作成される', () => {

      const postits = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit')
      postits.length.should.equal(1)
    })

    it('`title`クラスを持つ要素が1つ作成される', () => {

      const postits = TestUtils.scryRenderedDOMComponentsWithClass(root, 'title')
      postits.length.should.equal(1)
    })

    it('titleがレンダリングされる', () => {

      const postits = TestUtils
        .scryRenderedDOMComponentsWithClass(root, 'title')
        .map(comp => comp.textContent)
      postits[0].should.equal('カードのタイトル')
    })

    it('`content`クラスを持つ要素が1つ作成される', () => {

      const contents = TestUtils.scryRenderedDOMComponentsWithClass(root, 'content')
      contents.length.should.equal(1)
    })

    it('contentがレンダリングされる', () => {

      const contents = TestUtils
        .scryRenderedDOMComponentsWithClass(root, 'content')
        .map(comp => comp.textContent)
      contents[0].should.equal('カードのコンテンツ')
    })

    it('`button-confirm`クラスを持つ要素はがレンダリングされる', () => {

      const buttons = TestUtils.scryRenderedDOMComponentsWithClass(root, 'button-confirm')
      buttons.length.should.equal(1)
    })

    it('`button-confirm`クラスを持つ要素は、クリックされるとconfirmPostitアクションを実行する', () => {

      const button = TestUtils.scryRenderedDOMComponentsWithClass(root, 'button-confirm')[0]
      confirmed.should.be.False()
      TestUtils.Simulate.click(button)
      confirmed.should.be.True()
    })

    it('Drag中はdraggingクラスを持つ', () => {
      // Find the drag source ID and use it to simulate the dragging operation
      const postitContext = TestUtils.findRenderedComponentWithType(root, _Postit)
      backend.simulateBeginDrag([postitContext.getHandlerId()])

      const postit = TestUtils.findRenderedDOMComponentWithClass(root, 'postit')
      postit.className.should.containEql('dragging')
      backend.simulateEndDrag()
    })

    it.skip('Drag中に他のDropContext[type=postit]にホバーするとswapPostitアクションを呼ぶ', () => {
      // Find the drag source ID and use it to simulate the dragging operation
      const postitContext = TestUtils.findRenderedComponentWithType(root, _Postit)
      swapped = false
      backend.simulateBeginDrag([postitContext.getHandlerId()])
      // TODO: hoverをシミュレートしたいが、TargetIDが不正を怒られてしまう
      backend.simulateHover([postitContext.getHandlerId()])
      swapped.should.True()
      backend.simulateEndDrag()
    })

  })

  describe('編集領域表示(toggled)のテスト', () => {

    let Postit = null
    let root = null
    let toggled = false

    beforeEach(() => {

      // Render with the test context that uses the test backend
      Postit = wrapInTestContext(_Postit)
      const actionHandlers = createEmptyActionHandlers()
      actionHandlers.togglePostit = () => () => toggled = true

      root = TestUtils.renderIntoDocument(
        <Postit
          id='1'
          title={ { value: 'カードのタイトル' } }
          content={ { value: 'カードのコンテンツ' } }
          actionHandlers={ actionHandlers } />
      )
    })

    it('`button-toggle`クラスを持つ要素は、クリックされるとtogglePostitアクションを実行する', () => {

      const button = TestUtils.scryRenderedDOMComponentsWithClass(root, 'button-toggle')[0]
      toggled.should.be.False()
      TestUtils.Simulate.click(button)
      toggled.should.be.True()
    })
  })

  describe('編集領域非表示(collapsed)のテスト', () => {

    let Postit = null
    let root = null
    let toggled = true

    beforeEach(() => {

      // Render with the test context that uses the test backend
      Postit = wrapInTestContext(_Postit)
      const actionHandlers = createEmptyActionHandlers()
      actionHandlers.togglePostit = () => () => toggled = true

      root = TestUtils.renderIntoDocument(
        <Postit
          id='1'
          title={ { value: 'カードのタイトル' } }
          content={ { value: 'カードのコンテンツ' } }
          toggled={ toggled }
          actionHandlers={ actionHandlers } />
      )
    })

    it('button-collapseクラスを持つ要素がある', () => {

      const buttons = TestUtils.scryRenderedDOMComponentsWithClass(root, 'button-collapse')
      buttons.length.should.equal(1)
    })
  })

  describe('確定(confirmed)なカード', () => {

    let Postit
    let actionHandlers

    beforeEach(() => {

      // Render with the test context that uses the test backend
      Postit = wrapInTestContext(_Postit)
      actionHandlers = createEmptyActionHandlers()
    })

    it('confirmedな`Postit`コンポーネントはconfirmedクラスを持つ', () => {

      const wrapper = render(<Postit
        id='1'
        title={ { value: 'カードのタイトル' } }
        content={ { value: 'カードのコンテンツ' } }
        confirmed={ true }
        actionHandlers={ actionHandlers } />)

      expect(wrapper.children().children().is('.confirmed')).to.be.true
    })
  })
  // See other backend.simulate* methods for more!
})
