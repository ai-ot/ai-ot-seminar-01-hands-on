/**
 * Test with React DnD
 * https://gaearon.github.io/react-dnd/docs-testing.html
 */

 /**
 * テスト補助環境
 */
import done                      from '../helpers/initializeDOM'
import wrapInTestContext         from '../helpers/DnD/wrapInTestContext'
import createEmptyActionHandlers from '../helpers/createEmptyActionHandlers'
import TestUtils                 from 'react-addons-test-utils'
import                                'should'
/**
 * React本体
 */
import React from 'react'
/**
 * テスト対象
 */
import _PostitEmpty from '../../../src/components/PostitEmpty.jsx'

describe('PostitEmptyコンポーネントのテスト', () => {

  describe('PostitEmptyコンポーネント単体へのテスト', () => {

    let PostitEmpty = null
    let root = null
    let backend = null
    let swapped = false

    beforeEach(() => {

      // Render with the test context that uses the test backend
      PostitEmpty = wrapInTestContext(_PostitEmpty)
      const actionHandlers = createEmptyActionHandlers()
      actionHandlers.movePostit = () => () => swapped = true
      root = TestUtils.renderIntoDocument(
        <PostitEmpty
          id='1'
          listId='listId'
          actionHandlers={ actionHandlers } />
      )

      // Obtain a reference to the backend
      backend = root.getManager().getBackend()
    })

    it('`postit`クラスを持つ要素が1つ作成される', () => {
      const postits = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit')
      postits.length.should.equal(1)
    })

    it('`drop-target`クラスを持つ要素が1つ作成される', () => {
      const postits = TestUtils.scryRenderedDOMComponentsWithClass(root, 'drop-target')
      postits.length.should.equal(1)
    })

    it('`postit-empty`クラスを持つ要素が1つ作成される', () => {
      const postits = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit-empty')
      postits.length.should.equal(1)
    })


    it.skip('Drag中に他のDropContext[type=postit]にホバーするとmovePostitアクションを呼ぶ', () => {
      // Find the drag source ID and use it to simulate the dragging operation
      const postitContext = TestUtils.findRenderedComponentWithType(root, _PostitEmpty)
      swapped = false
      backend.simulateBeginDrag([postitContext.getHandlerId()])
      // TODO: hoverをシミュレートしたいが、TargetIDが不正を怒られてしまう
      backend.simulateHover([postitContext.getHandlerId()])
      swapped.should.True()
      backend.simulateEndDrag()
    })

  })
  // See other backend.simulate* methods for more!
})
