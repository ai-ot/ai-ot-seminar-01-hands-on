import done              from '../helpers/initializeDOM'
import wrapInTestContext from '../helpers/DnD/wrapInTestContext'
import React             from 'react'
import TestUtils         from 'react-addons-test-utils'
import TimePicker        from 'rc-time-picker'
import { shallow, mount, render } from 'enzyme'
import { expect, should } from 'chai'

import createEmptyActionHandlers from '../helpers/createEmptyActionHandlers'
import _List from '../../../src/components/List.jsx'

describe('Listコンポーネントのテスト', () => {

  describe('Listコンポーネント単体でのテスト', () => {

    let List = null
    let root = null

    beforeEach(() => {

      // Render with the test context that uses the test backend
      List = wrapInTestContext(_List)
      root = TestUtils.renderIntoDocument(
        <List
          id='1'
          title={ { value: 'リストのタイトル' } }
          actionHandlers={ createEmptyActionHandlers() } />
      )
    })

    it('`list`クラスを持つ要素が1つ作成される', () => {

      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'list')
      element.length.should.equal(1)
    })

    it('`list-title`クラスを持つ要素が1つ作成される', () => {

      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'list-title')
      element.length.should.equal(1)
    })

    it('`postit-empty`クラスを持つ要素が1つ作成される', () => {
      // TODO: 本来ならshallowレンダリングしてPostitEmptyコンポーネントが得られることをテストするべきだ
      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit-empty')
      element.length.should.equal(1)
    })
  })

  describe('内部のPostitコンポーネントのテスト', () => {
    let List = null
    let root = null

    beforeEach(() => {

      // Render with the test context that uses the test backend
      List = wrapInTestContext(_List)
      root = TestUtils.renderIntoDocument(
        <List
          id='1'
          title={ { value: 'リストのタイトル' } }
          postits={ [ {
            id: 'postit1',
            title: { value: 'aaa' },
            content: { value: 'bbb' }
          }, {
            id: 'postit2',
            title: { value: 'aaa' },
            content: { value: 'bbb' }
          } ] }
          actionHandlers={ createEmptyActionHandlers() } />
      )
    })


    it('`postit-empty`クラスを持つ要素が作成されない', () => {
      // TODO: 本来ならshallowレンダリングしてPostitEmptyコンポーネントが得られることをテストするべきだ
      const element = TestUtils.scryRenderedDOMComponentsWithClass(root, 'postit-empty')
      element.length.should.equal(0)
    })
  })

  describe('確定(confirmed)なカードを持つリスト', () => {

    let List
    let actionHandlers

    beforeEach(() => {

      List = wrapInTestContext(_List)
      actionHandlers = createEmptyActionHandlers()
    })

    it('confirmedなpostitを持つListはhas-confirmedクラスを持つ', () => {

      const wrapper = render(<List
        id='1'
        title={ { value: 'リストのタイトル' } }
        postits={ [ {
          id: 'postit1',
          title: { value: 'aaa' },
          content: { value: 'bbb' },
          confirmed: true
        } ] }
        actionHandlers={ actionHandlers } />)

      wrapper.children().is('.has-confirmed').should.True()
    })
  })

  describe('`leftAt`プロパティを持つリスト', () => {

    let List
    let actionHandlers

    beforeEach(() => {

      List = wrapInTestContext(_List)
      actionHandlers = createEmptyActionHandlers()
    })

    it.skip('`leftAt`プロパティを持つListはTimePickerコンポーネントを持つ', () => {
      // TODO: enzyme.shallowがよくわかりません
      // TimePickerがレンダリングされるみたいなのをテストしたいです
      const wrapper = render(<List
        id='1'
        actionHandlers={ actionHandlers } />)

      expect(wrapper.find('.rc-time-picker.left-at')).to.have.length(1)
    })
  })
})
