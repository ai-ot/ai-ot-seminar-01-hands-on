import 'should'
import deletePostit from '../../../src/actions/deletePostit'

describe('deletePostitアクションのテスト', () => {

  it('idを指定するとpostitオブジェクトを削除できる', () => {

    const id = 'deleting-postit-id'
    const state = {
      lists: [ { postits: [
        { id }
      ] } ]
    }
    const postitsActual = deletePostit.func(state, { id }).lists[0].postits
    postitsActual.length.should.be.equal(0)
  })

  it('dockの中のpostitオブジェクトを削除できる', () => {

    const id = 'deleting-postit-id'
    const state = {
      dock: { postits: [
        { id }
      ] }
    }
    const postitsActual = deletePostit.func(state, { id }).dock.postits
    postitsActual.length.should.be.equal(0)
  })

  it('idが見つからない時は、stateがそのまま帰ってくる', () => {

    const id = 'deleting-postit-id'
    const state = {
      lists: [ { postits: [
        { id }
      ] } ]
    }
    deletePostit.func(state, { id: 'id-not-matched' }).should.be.deepEqual(state)
    deletePostit.func(state, { id: 'id-not-matched' }).should.be.equal(state)
  })


  it('コールバックが実行される', done => {

    const id = 'callingback-deleting-postit-id'
    const state = {
      lists: [ { postits: [
        { id }
      ] } ]
    }
    deletePostit.func(state, {
      id,
      callback: deleted => {
        deleted.id.should.equal(id)
        done()
      }
    } )
  })
})
