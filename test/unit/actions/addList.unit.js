import 'should'
import addList from '../../../src/actions/addList'
import {
  getInitialBoard,
  getInitialList
} from '../../../src/constants'

describe('addListアクションのテスト', () => {

  it('listオブジェクトでstateが更新される', () => {
    const state = getInitialBoard()
    const list = {
      id: 'list-id-test-1',
      title: { value: 'テストのリスト' },
    }
    const listActual = addList.func(state, { list }).lists[0]
    listActual.should.deepEqual(list)
  })

  it('listのidがない場合は自動で振られる', () => {
    const state = getInitialBoard()
    const list = {
      title: { value: 'テストのリスト' }
    }
    const listActual = addList.func(state, { list }).lists[0]
    listActual.id.should.not.be.Undefined()
  })

  it('listオブジェクトを指定しなかった場合、デフォルトのリストが追加される', () => {
    const state = getInitialBoard()
    const listActual = addList.func(state).lists[0]
    listActual.title.should.deepEqual(getInitialList().title)
    listActual.postits.length.should.equal(0)
  })

  it('追加されるリストは同一のオブジェクトではない', () => {
    const state = getInitialBoard()
    const state1 = addList.func(state)
    const state2 = addList.func(state1)
    state2.lists.length.should.equal(2)
    state2.lists[0].postits.should.not.equal(state2.lists[1].postits)
  })
})
