import 'should'
import endEdit from '../../../src/actions/endEdit'

describe('endEditアクション', () => {

  describe('Board Titleのケース', () => {

    let result

    before(done => {
      result = endEdit.func({ title: { editting: true } }, {
        type: 'BOARD_TITLE',
        value: 'test board title'
      })
      done()
    })

    it('編集が終了できることを確認します', () => {
      result.title.editting.should.be.False()
    })

    it('値が編集されていることを確認します', () => {
      result.title.value.should.equal('test board title')
    })
  })

  describe('List Titleのケース', () => {

    let result

    describe('処理成功', () => {

      before(done => {
        result = endEdit.func({
          lists: [{ title: { editting: true }, id: 123 }]
        }, {
          type: 'LIST_TITLE',
          id: 123,
          value: 'test list title'
        })
        done()
      })

      it('編集が終了できることを確認します', () => {
        result.lists[0].title.editting.should.be.False()
      })

      it('値が編集されていることを確認します', () => {
        result.lists[0].title.value.should.equal('test list title')
      })
    })

    describe('処理失敗', () => {

      const state = { lists: [{ id: 123 }] }

      before(done => {
        result = endEdit.func(state, {
          type: 'LIST_TITLE',
          id: 1,
          value: 'test list title initial value'
        })
        done()
      })

      it('idが見つからなかった時はそのままstateを返します。', () => {
        result.should.be.equal(state)
      })
    })
  })

  describe('Dock外のPostit Titleのケース', () => {

    let result

    before(done => {
      result = endEdit.func({
        lists: [{
          postits: [{
            title: { editting: true },
            id: 234
          }]
        }]
      }, {
        type: 'CARD_TITLE',
        id: 234,
        value: 'test postit title'
      })
      done()
    })

    it('編集が終了できることを確認します', () => {
      result.lists[0].postits[0].title.editting.should.be.False()
    })

    it('titleの値が編集されていることを確認します', () => {
      result.lists[0].postits[0].title.value.should.equal('test postit title')
    })
  })

  describe('Dock外のPostit Contentのケース', () => {

    let result

    before(done => {
      result = endEdit.func({
        lists: [{
          postits: [{
            content: { editting: true },
            id: 345
          }]
        }]
      }, {
        type: 'CARD_CONTENT',
        id: 345,
        value: 'test postit content'
      })
      done()
    })

    it('編集が終了できることを確認します', () => {
      result.lists[0].postits[0].content.editting.should.be.False()
    })

    it('値が編集されていることを確認します', () => {
      result.lists[0].postits[0].content.value.should.equal('test postit content')
    })
  })

  describe('Dock内のPostit Titleのケース', () => {

    let result

    before(done => {
      result = endEdit.func({
        dock: { postits: [{ id: '345' }] }
      }, {
        type: 'CARD_TITLE',
        id: '345',
        value: 'test postit title initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.dock.postits[0].title.editting.should.be.False()
    })

    it('初期値が設定されていることを確認します', () => {
      result.dock.postits[0].title.value.should.equal('test postit title initial value')
    })
  })

  describe('Dock内のPostit Contentのケース', () => {

    let result

    before(done => {
      result = endEdit.func({
        dock: { postits: [{ id: '345' }] }
      }, {
        type: 'CARD_CONTENT',
        id: '345',
        value: 'test postit content initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.dock.postits[0].content.editting.should.be.False()
    })

    it('初期値が設定されていることを確認します', () => {
      result.dock.postits[0].content.value.should.equal('test postit content initial value')
    })
  })

  describe('知らないTYPEが指定されたケース', () => {

    let result
    let state = {}

    before(done => {
      result = endEdit.func(state, { type: 'TYPE_NOT_DOUND' })
      done()
    })

    it('知らないTYPEが指定されたときはそのままstateを返す', () => {
      result.should.equal(state)
    })
  })


  describe('dockプロパティ', () => {

    [
      'BOARD_TITLE',
      'LIST_TITLE',
      'CARD_TITLE',
      'CARD_CONTENT'
    ].map(type => {
      it('type:' + type + ' に対して、dockプロパティが失われない', () => {
        const state = { lists: [{ id: 'a' }], dock: {} }
        const result = endEdit.func(state, { type })
        result.dock.should.not.be.Undefined()
      })
    })
  })
})
