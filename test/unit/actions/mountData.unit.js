import 'should'
import mountData from '../../../src/actions/mountData'

describe('mountDataアクションのテスト', () => {

  it('stateをそのまま返す', () => {
    const state = { some: 'state' }
    mountData.func({}, { state }).should.deepEqual({ some: 'state' })
  })
})
