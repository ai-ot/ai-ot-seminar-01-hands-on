import 'should'
import confirmPostit from '../../../src/actions/confirmPostit'

describe('confirmPostitアクションのテスト', () => {

  describe('lists内部', () => {

    let state

    beforeEach(() => {
      state = { lists: [{ postits: [{ id: 'the-postit' }] }] }
    })

    it('confirmedプロパティをtrueで付加できる', () => {
      const newState = confirmPostit.func(state, { id: 'the-postit', confirmed: true })
      newState.lists[0].postits[0].confirmed.should.be.True()
    })

    it('confirmedプロパティをfalseで付加できる', () => {
      const newState = confirmPostit.func(state, { id: 'the-postit', confirmed: false })
      newState.lists[0].postits[0].confirmed.should.be.False()
    })
  })

  describe('dock内部', () => {
    let state

    beforeEach(() => {
      state = { dock: { postits: [{ id: 'the-postit' }] } }
    })

    it('confirmedプロパティをtrueで付加できる', () => {
      const newState = confirmPostit.func(state, { id: 'the-postit', confirmed: true })
      newState.dock.postits[0].confirmed.should.be.True()
    })

    it('confirmedプロパティをfalseで付加できる', () => {
      const newState = confirmPostit.func(state, { id: 'the-postit', confirmed: false })
      newState.dock.postits[0].confirmed.should.be.False()
    })
  })

  describe('異常系', () => {
    let state

    beforeEach(() => {
      state = { lists: [{ postits: [{ id: 'the-postit' }] }] }
    })

    it('idが見つからなければそのまま', () => {
      const newState = confirmPostit.func(state, { id: 'not-found', confirmed: true })
      newState.should.deepEqual({ lists: [{ postits: [{ id: 'the-postit' }] }] })
    })
  })
})
