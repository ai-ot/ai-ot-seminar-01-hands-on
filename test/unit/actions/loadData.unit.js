import 'should'
import createMockServer from '../../../server/createMockServer'
import loadData from '../../../src/actions/loadData'

const serverConf = {
  port: 2998,
  quiet: true
}
const url = `http://localhost:${serverConf.port}/storage.json`

const quiet = true

describe('loadDataアクション', () => {

  it('jsonがロードされ、所定の形を持っていることを確認します', done => {

    createMockServer(serverConf)
      .then(() => {
        loadData.func({}, {
          url,
          quiet,
          callback: data => {
            data.title.should.not.be.Undefined()
            data.title.should.be.an.Object()
            data.title.value.should.be.a.String()
            data.dock.should.not.be.Undefined()
            data.dock.title.value.should.be.a.String()
            done()
          }
        })
      })
  })
})
