import 'should'
import movePostit from '../../../src/actions/movePostit'
import {
  getInitialBoard,
  getInitialList
} from '../../../src/constants'

describe('movePostitアクションのテスト', () => {

  let state = null
  let list1 = null
  let list2 = null

  beforeEach(() => {
    state = Object.assign({}, getInitialBoard())
    list1 = Object.assign({}, getInitialList())
    list2 = Object.assign({}, getInitialList())
    list1.id = 'list1'
    list2.id = 'list2'
    list1.postits = []
    list2.postits = []
    state.lists = [list1, list2]
    state = {
      lists: [
        {
          id: 'list1',
          postits: [ { id: 'postit1-1' } ]
        },
        {
          id: 'list2',
          postits: [ { id: 'postit2-1' } ]
        }
      ],
      dock: {
        id: 'the-dock',
        postits: [{ id: 'postit3-1' }]
      }
    }

  })

  it('カードが別のリストに移動される', () => {
    const newState = movePostit.func(state, { postitId: 'postit2-1', listId: 'list1' })
    newState.lists[1].postits.length.should.equal(0)
    newState.lists[0].postits[1].id.should.equal('postit2-1')
  })

  it('カードがリストからdockに移動される', () => {
    const newState = movePostit.func(state, { postitId: 'postit1-1', listId: 'the-dock' })
    newState.lists[0].postits.length.should.equal(0)
    newState.dock.postits.length.should.equal(2)
    newState.dock.postits[1].id.should.equal('postit1-1')
  })

  it('カードがdockからリストに移動される', () => {
    const newState = movePostit.func(state, { postitId: 'postit3-1', listId: 'list1' })
    newState.lists[0].postits.length.should.equal(2)
    newState.dock.postits.length.should.equal(0)
    newState.lists[0].postits[1].id.should.equal('postit3-1')
  })

  it('postitIDが見つからなかったときにはそのままstateを返す', () => {
    const newState = movePostit.func(state, { postitId: 'not-found', listId: 'list1' })
    newState.should.equal(state)
  })

  it('listIDが見つからなかったときにはそのままstateを返す', () => {
    const newState = movePostit.func(state, { postitId: 'postit3-1', listId: 'not-found' })
    newState.should.equal(state)
  })


})
