import 'should'
import updateTime from '../../../src/actions/updateTime'
import Moment from 'moment'

describe('updateTimeアクションのテスト', () => {

  describe('LEFT_ATタイプ', () => {

    let state
    let oldState

    beforeEach(() => {

      state = { lists: [ { id: 'the-list' } ] }
      oldState = { lists: [ { id: 'the-list' } ] } // deepCopied
    })

    it('leftAtプロパティを更新する', () => {
      const newState = updateTime.func(state, {
        id: 'the-list',
        type: 'LEFT_AT',
        value: 1483593209173
      } )
      newState.lists[0].leftAt.should.equal(1483593209173)
    })

    it('leftAtプロパティをMomentインスタンスで更新する', () => {
      const newState = updateTime.func(state, {
        id: 'the-list',
        type: 'LEFT_AT',
        value: new Moment('2017-01-05T14:13:29.173')
      } )
      newState.lists[0].leftAt.should.equal(1483593209173)
    })


    it('idが見つからない場合はstateは変化しない', () => {
      const newState = updateTime.func(state, {
        id: 'list-not-found',
        type: 'LEFT_AT',
        value: 1483593209173
      } )
      newState.should.equal(state)
      newState.should.deepEqual(oldState)
    })
  })

  describe('START_ATタイプ', () => {

    let state
    let oldState

    beforeEach(() => {

      state = { lists: [ { id: 'the-list' } ] }
      oldState = { lists: [ { id: 'the-list' } ] } // deepCopied
    })

    it('startAtプロパティを更新する', () => {
      const newState = updateTime.func(state, {
        id: 'the-list',
        type: 'START_AT',
        value: 1483593209173
      } )
      newState.lists[0].startAt.should.equal(1483593209173)
    })

    it('startAtプロパティをMomentインスタンスで更新する', () => {
      const newState = updateTime.func(state, {
        id: 'the-list',
        type: 'START_AT',
        value: new Moment('2017-01-05T14:13:29.173')
      } )
      newState.lists[0].startAt.should.equal(1483593209173)
    })

    it('idが見つからない場合はstateは変化しない', () => {
      const newState = updateTime.func(state, {
        id: 'list-not-found',
        type: 'START_AT',
        value: 1483593209173
      } )
      newState.should.equal(state)
      newState.should.deepEqual(oldState)
    })
  })

  describe('タイプ異常', () => {

    let state
    let oldState

    beforeEach(() => {

      state = { lists: [ { id: 'the-list' } ] }
      oldState = { lists: [ { id: 'the-list' } ] } // deepCopied
    })

    it('誤ったタイプを指定すると更新されない', () => {
      const newState = updateTime.func(state, {
        id: 'the-list',
        type: '_TYPE_NOT_FOUND_CASE_TEST',
        value: 1483593209173
      } )
      newState.should.equal(state)
      newState.should.deepEqual(oldState)
    })
  })
})
