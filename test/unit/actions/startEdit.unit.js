import 'should'
import startEdit from '../../../src/actions/startEdit'

describe('startEditアクション', () => {

  describe('Board Titleのケース', () => {

    let result

    before(done => {
      result = startEdit.func({}, {
        type: 'BOARD_TITLE',
        value: 'test board title initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.title.editting.should.be.True()
    })

    it('初期値が設定されていることを確認します', () => {
      result.title.value.should.equal('test board title initial value')
    })
  })

  describe('List Titleのケース', () => {

    let result

    describe('処理成功', () => {

      before(done => {
        result = startEdit.func({
          lists: [{ id: 123 }]
        }, {
          type: 'LIST_TITLE',
          id: 123,
          value: 'test list title initial value'
        })
        done()
      })

      it('編集が開始できることを確認します', () => {
        result.lists[0].title.editting.should.be.True()
      })

      it('初期値が設定されていることを確認します', () => {
        result.lists[0].title.value.should.equal('test list title initial value')
      })
    })

    describe('処理失敗', () => {

      const state = { lists: [{ id: 123 }] }

      before(done => {
        result = startEdit.func(state, {
          type: 'LIST_TITLE',
          id: 1,
          value: 'test list title initial value'
        })
        done()
      })

      it('idが見つからなかった時はそのままstateを返します。', () => {
        result.should.be.equal(state)
      })
    })
  })

  describe('Dock外のPostit Titleのケース', () => {

    let result

    before(done => {
      result = startEdit.func({
        lists: [{
          postits: [{
            id: 234
          }]
        }]
      }, {
        type: 'CARD_TITLE',
        id: 234,
        value: 'test postit title initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.lists[0].postits[0].title.editting.should.be.True()
    })

    it('初期値が設定されていることを確認します', () => {
      result.lists[0].postits[0].title.value.should.equal('test postit title initial value')
    })
  })

  describe('Dock外のPostit Contentのケース', () => {

    let result

    before(done => {
      result = startEdit.func({
        lists: [{
          postits: [{
            id: 345
          }]
        }]
      }, {
        type: 'CARD_CONTENT',
        id: 345,
        value: 'test postit content initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.lists[0].postits[0].content.editting.should.be.True()
    })

    it('初期値が設定されていることを確認します', () => {
      result.lists[0].postits[0].content.value.should.equal('test postit content initial value')
    })
  })

  describe('Dockn内のPostit Titleのケース', () => {

    let result

    before(done => {
      result = startEdit.func({
        dock: { postits: [{ id: '345' }] }
      }, {
        type: 'CARD_TITLE',
        id: '345',
        value: 'test postit title initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.dock.postits[0].title.editting.should.be.True()
    })

    it('初期値が設定されていることを確認します', () => {
      result.dock.postits[0].title.value.should.equal('test postit title initial value')
    })
  })

  describe('Dock内のPostit Contentのケース', () => {

    let result

    before(done => {
      result = startEdit.func({
        dock: { postits: [{ id: '345' }] }
      }, {
        type: 'CARD_CONTENT',
        id: '345',
        value: 'test postit content initial value'
      })
      done()
    })

    it('編集が開始できることを確認します', () => {
      result.dock.postits[0].content.editting.should.be.True()
    })

    it('初期値が設定されていることを確認します', () => {
      result.dock.postits[0].content.value.should.equal('test postit content initial value')
    })
  })

  describe('知らないTYPEが指定されたケース', () => {

    let result
    let state = {}

    before(done => {
      result = startEdit.func(state, { type: 'TYPE_NOT_DOUND' })
      done()
    })

    it('知らないTYPEが指定されたときはそのままstateを返す', () => {
      result.should.equal(state)
    })
  })

  describe('dockプロパティ', () => {

    [
      'BOARD_TITLE',
      'LIST_TITLE',
      'CARD_TITLE',
      'CARD_CONTENT'
    ].map(type => {
      it('type:' + type + ' に対して、dockプロパティが失われない', () => {
        const state = { lists: [{ id: 'a' }], dock: {} }
        const result = startEdit.func(state, { type })
        result.dock.should.not.be.Undefined()
      })
    })
  })
})
