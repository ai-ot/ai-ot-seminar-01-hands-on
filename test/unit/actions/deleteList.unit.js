import 'should'
import deleteList from '../../../src/actions/deleteList'

describe('deleteListアクションのテスト', () => {

  it('idを指定するとlistオブジェクトを削除できる', () => {

    const id = 'deleting-list-id'
    const state = {
      lists: [ { id } ]
    }
    const listsActual = deleteList.func(state, { id }).lists
    listsActual.length.should.be.equal(0)
  })

  it('idが見つからない時は、stateがそのまま帰ってくる', () => {

    const id = 'deleting-list-id'
    const state = {
      lists: [ { id } ]
    }
    deleteList.func(state, { id: 'id-not-matched' }).should.be.deepEqual(state)
    deleteList.func(state, { id: 'id-not-matched' }).should.be.equal(state)
  })
})
