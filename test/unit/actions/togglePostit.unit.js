import 'should'
import togglePostit from '../../../src/actions/togglePostit'

describe('togglePostitアクションのテスト', () => {

  describe('dockに入っていないケース', () => {

    describe('toggled: trueのケース', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          lists: [ { postits: [
            { id }
          ] } ]
        }
        newState = togglePostit.func(oldState, { id, toggle: true })
      })

      it('idを指定するとpostitオブジェクトにtoggled: trueプロパティを付与する', () => {
        newState.lists[0].postits[0].toggled.should.be.True()
      })

      it('その親にもtoggled: trueプロパティを付与する', () => {
        newState.lists[0].toggled.should.be.True()
      })
    })

    describe('toggled: falseのケース', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          lists: [ { postits: [
            { id }
          ] } ]
        }
        newState = togglePostit.func(oldState, { id, toggle: false })
      })

      it('idを指定するとpostitオブジェクトにtoggled: trueプロパティを付与する', () => {
        newState.lists[0].postits[0].toggled.should.be.False()
      })

      it('その親にもtoggled: trueプロパティを付与する', () => {
        newState.lists[0].toggled.should.be.False()
      })
    })

    describe('見つからないカードを指定した時', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          lists: [ { postits: [
            { id: 'id-not-found' }
          ] } ]
        }
        newState = togglePostit.func(oldState, { id, toggle: false })
      })

      it('指定したidが見つからなければ元のstateを返す', () => {
        newState.should.be.deepEqual(oldState)
        newState.should.be.equal(oldState)
      })
    })
  })

  describe('dockに入っているケース', () => {

    describe('toggled: trueのケース', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          dock: { postits: [
            { id }
          ] }
        }
        newState = togglePostit.func(oldState, { id, toggle: true })
      })

      it('idを指定するとpostitオブジェクトにtoggled: trueプロパティを付与する', () => {
        newState.dock.postits[0].toggled.should.be.True()
      })

      it('その親にもtoggled: trueプロパティを付与する', () => {
        newState.dock.toggled.should.be.True()
      })
    })

    describe('toggled: falseのケース', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          dock: { postits: [
            { id }
          ] }
        }
        newState = togglePostit.func(oldState, { id, toggle: false })
      })

      it('idを指定するとpostitオブジェクトにtoggled: trueプロパティを付与する', () => {
        newState.dock.postits[0].toggled.should.be.False()
      })

      it('その親にもtoggled: trueプロパティを付与する', () => {
        newState.dock.toggled.should.be.False()
      })
    })

    describe('見つからないカードを指定した時', () => {

      let id = null
      let oldState = null
      let newState = null

      before(() => {
        id = 'toggling-postit-id'
        oldState = {
          dock: { postits: [
            { id: 'id-not-found' }
          ] }
        }
        newState = togglePostit.func(oldState, { id, toggle: false })
      })

      it('指定したidが見つからなければ元のstateを返す', () => {
        newState.should.be.deepEqual(oldState)
        newState.should.be.equal(oldState)
      })
    })
  })
})
