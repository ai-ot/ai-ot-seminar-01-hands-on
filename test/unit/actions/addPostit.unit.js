import 'should'
import addPostit from '../../../src/actions/addPostit'
import {
  getInitialBoard,
  getInitialList,
  getInitialPostit
} from '../../../src/constants'

describe('addPostitアクションのテスト', () => {

  describe('通常のリストにカードを追加する', () => {

    let state = {}
    let list = {}
    let id = ''

    beforeEach(done => {
      state = Object.assign({}, getInitialBoard())
      list = Object.assign({}, getInitialList())
      id = list.id = 'new-list-id'
      list.postits = []
      state.lists = [list]
      done()
    })

    it('postitオブジェクトでstateが更新される', () => {
      const postit = {
        id: 'postit-id-test-1',
        title: { value: 'テストのカード' },
        content: { value: 'テストのカードコンテント' }
      }
      const postitActual = addPostit.func(state, { id, postit }).lists[0].postits[0]
      postitActual.should.deepEqual(postit)
    })

    it('postitのidがない場合は自動で振られる', () => {

      const postit = {
        title: { value: 'テストのカード2' },
        content: { value: 'テストのカードコンテント2' }
      }
      const postitActual = addPostit.func(state, { id, postit }).lists[0].postits[0]
      postitActual.id.should.not.be.Undefined()
    })

    it('postitオブジェクトを指定しなかった場合、デフォルトのカードが追加される', () => {
      const postitActual = addPostit.func(state, { id }).lists[0].postits[0]
      postitActual.title.should.deepEqual(getInitialPostit().title)
      postitActual.content.should.deepEqual(getInitialPostit().content)
    })

    it('idが見つからなかった時は、そのままのstateを返す', () => {
      addPostit.func(state, { id: 'id-not-found' }).should.equal(state)
    })
  })

  describe('dockにカードを追加する', () => {

    let state = {}
    let dock = {}
    let id = ''

    beforeEach(done => {
      state = Object.assign({}, getInitialBoard())
      dock = Object.assign({}, getInitialList())
      id = dock.id = 'dock'
      dock.postits = []
      state.dock = dock
      done()
    })

    it('postitオブジェクトでstateが更新される', () => {
      const postit = {
        id: 'postit-id-test-1',
        title: { value: 'テストのカード' },
        content: { value: 'テストのカードコンテント' }
      }
      const postitActual = addPostit.func(state, { id, postit }).dock.postits[0]
      postitActual.should.deepEqual(postit)
    })

    it('postitのidがない場合は自動で振られる', () => {

      const postit = {
        title: { value: 'テストのカード2' },
        content: { value: 'テストのカードコンテント2' }
      }
      const postitActual = addPostit.func(state, { id, postit }).dock.postits[0]
      postitActual.id.should.not.be.Undefined()
    })

    it('postitsプロパティがなくてもカードを追加できる', () => {
      state.dock.postits = undefined
      const postit = {
        title: { value: 'テストのカード3' },
        content: { value: 'テストのカードコンテント3' }
      }
      const postitActual = addPostit.func(state, { id, postit }).dock.postits[0]
      postitActual.id.should.not.be.Undefined()
    })

    it('postitオブジェクトを指定しなかった場合、デフォルトのカードが追加される', () => {
      const postitActual = addPostit.func(state, { id }).dock.postits[0]
      postitActual.title.should.deepEqual(getInitialPostit().title)
      postitActual.content.should.deepEqual(getInitialPostit().content)
    })
  })
})
