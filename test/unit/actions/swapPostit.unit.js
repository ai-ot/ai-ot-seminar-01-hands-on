import 'should'
import swapPostit from '../../../src/actions/swapPostit'

describe('swapPostitアクションのテスト', () => {

  it('リスト内でカードを交換できる', () => {

    const oldState = { lists: [{ postits: [{ id: 'postit1' }, { id: 'postit2' }] }] }
    const newState = swapPostit.func(oldState, { idFrom: 'postit1', idInto: 'postit2' })
    newState.lists[0].postits[0].id.should.equal('postit2')
    newState.lists[0].postits[1].id.should.equal('postit1')
  })

  it('リスト間でカードを交換を行った場合、交換先の位置に挿入される', () => {

    const oldState = { lists: [
      { id: 'list1', postits: [{ id: 'postit1' }] },
      { id: 'list2', postits: [{ id: 'postit2' }] }
    ] }
    const newState = swapPostit.func(oldState, { idFrom: 'postit1', idInto: 'postit2' })
    newState.lists[0].postits.length.should.equal(0)
    newState.lists[1].postits[0].id.should.equal('postit1')
    newState.lists[1].postits[1].id.should.equal('postit2')
  })

  it('リスト->dockで交換を行った場合、交換先の位置に挿入される', () => {

    const oldState = {
      lists: [{ postits: [{ id: 'postit1' }] }],
      dock: { postits: [{ id: 'postit2' }] }
    }
    const newState = swapPostit.func(oldState, { idFrom: 'postit1', idInto: 'postit2' })
    newState.dock.postits.length.should.equal(2)
    newState.lists[0].postits.length.should.equal(0)
    newState.dock.postits[0].id.should.equal('postit1')
    newState.dock.postits[1].id.should.equal('postit2')
  })

  it('dock->リストで交換を行った場合、交換先の位置に挿入される', () => {

    const oldState = {
      lists: [{ postits: [{ id: 'postit1' }] }],
      dock: { postits: [{ id: 'postit2' }] }
    }
    const newState = swapPostit.func(oldState, { idFrom: 'postit2', idInto: 'postit1' })
    newState.dock.postits.length.should.equal(0)
    newState.lists[0].postits.length.should.equal(2)
    newState.lists[0].postits[0].id.should.equal('postit2')
    newState.lists[0].postits[1].id.should.equal('postit1')
  })

  it('dock内でカードを交換できる', () => {

    const oldState = { dock: { postits: [{ id: 'postit1' }, { id: 'postit2' }] } }
    const newState = swapPostit.func(oldState, { idFrom: 'postit1', idInto: 'postit2' })
    newState.dock.postits[0].id.should.equal('postit2')
    newState.dock.postits[1].id.should.equal('postit1')
  })

  it('同一のidを指定した場合はそのままstateを返す', () => {

    const oldState = { dock: { postits: [{ id: 'postit1' }, { id: 'postit2' }] } }
    const newState = swapPostit.func(oldState, { idFrom: 'postit1', idInto: 'postit1' })
    newState.should.equal(oldState)
  })

})
