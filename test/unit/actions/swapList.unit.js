import 'should'
import swapList from '../../../src/actions/swapList'

describe('swapListアクションのテスト', () => {

  it('リストを交換できる', () => {

    const oldState = { lists: [{ id: 'list1' }, { id: 'list2' }] }
    const newState = swapList.func(oldState, { idFrom: 'list1', idInto: 'list2' })
    newState.lists[0].id.should.equal('list2')
    newState.lists[1].id.should.equal('list1')
  })

  it('同じIDを指定された場合はそのままstateを返す', () => {

    const oldState = {
      lists: [{ id: 'list1' }, { id: 'list2' }],
      dock: { id: 'dock' }
    }
    const newState = swapList.func(oldState, { idFrom: 'list1', idInto: 'list1' })
    newState.should.equal(oldState)
    newState.lists[0].id.should.equal('list1')
    newState.lists[1].id.should.equal('list2')
  })

  it('dockプロパティは失われない', () => {

    const oldState = {
      lists: [{ id: 'list1' }, { id: 'list2' }],
      dock: { id: 'dock' }
    }
    const newState = swapList.func(oldState, { idFrom: 'list1', idInto: 'list2' })
    newState.dock.should.deepEqual(oldState.dock)
  })
})
