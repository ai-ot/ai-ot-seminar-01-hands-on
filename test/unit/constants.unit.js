'use strinct'
import 'should'
import {
  getInitialBoard,
  getInitialList,
  getInitialPostit,
} from '../../src/constants'

describe('Boardオブジェクトの初期状態生成のテスト', () => {

  it('Boardオブジェクトが生成される', () => {
    const board = getInitialBoard()
    board.should.not.be.Undefined()
  })

  it('直下にtitleオブジェクトがある', () => {
    const title = getInitialBoard().title
    title.should.not.be.Undefined()
  })

  it('直下にDockオブジェクトがある', () => {
    const dock = getInitialBoard().dock
    dock.should.not.be.Undefined()
  })

  it('dockオブジェクトにIDが振られている', () => {
    const dock = getInitialBoard().dock
    dock.id.should.be.a.String()
    dock.id.should.not.equal('')
  })

  it('Boardオブジェクトが繰り返し生成されたときに、同一オブジェクトを返さない', () => {
    const board1 = getInitialBoard()
    const board2 = getInitialBoard()
    board1.should.not.equal(board2)
  })

  describe('Boardオブジェクトが繰り返し生成されたときに、配下のオブジェクトプロパティも同一オブジェクトとならない', () => {
    const board1 = getInitialBoard()
    const board2 = getInitialBoard()
    const properies = [
      'lists'
    ]

    properies.map(property => {
      it(`${property}プロパティ`, () => {
        board1[property].should.not.equal(board2[property])
      })
    })
  })
})

describe('Listオブジェクトの初期状態生成のテスト', () => {

  it('Listオブジェクトが生成される', () => {
    const list = getInitialList()
    list.title.should.not.be.Undefined()
  })

  it('Listオブジェクトが繰り返し生成されたときに、同一オブジェクトを返さない', () => {
    const list1 = getInitialList()
    const list2 = getInitialList()
    list1.should.not.equal(list2)
  })

  describe('Listオブジェクトが繰り返し生成されたときに、配下のオブジェクトプロパティも同一オブジェクトとならない', () => {
    const list1 = getInitialList()
    const list2 = getInitialList()
    const properies = [
      'postits'
    ]

    properies.map(property => {
      it(`${property}プロパティ`, () => {
        list1[property].should.not.equal(list2[property])
      })
    })
  })
})

describe('Postitオブジェクトの初期状態生成のテスト', () => {

  it('Postitオブジェクトが生成される', () => {
    const postit = getInitialPostit()
    postit.title.should.not.be.Undefined()
  })

  it('Postitオブジェクトが繰り返し生成されたときに、同一オブジェクトを返さない', () => {
    const postit1 = getInitialPostit()
    const postit2 = getInitialPostit()
    postit1.should.not.equal(postit2)
  })

  // describe('Postitオブジェクトが繰り返し生成されたときに、配下のオブジェクトプロパティも同一オブジェクトとならない', () => {
  //   const postit1 = getInitialPostit()
  //   const postit2 = getInitialPostit()
  //   const properies = [
  //     'postits'
  //   ]
  //
  //   properies.map(property => {
  //     it(`${property}プロパティ`, () => {
  //       postit1[property].should.not.equal(postit2[property])
  //     })
  //   })
  // })
})
