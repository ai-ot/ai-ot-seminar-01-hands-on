# React カードボードアプリ サンプル

## 開発環境のセットアップ

この項目では、プロジェクトに新たにジョインする方を対象に開発環境をセットアップする方法を解説します。

### Homebrewのインストール(オプション、Mac OSのみ)

プロジェクトの開発やビルドに必要となる各種のツールをインストールするため、Mac OSにパッケージマネージャーをインストールします。

#### Homebrew本体のインストール

パッケージマネージャーの[Homebrew](http://brew.sh/index_ja.html)をインストールします。
公式でインストールスクリプトが提供されています。

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
$ brew doctor
$ brew update
```

#### Homebrew-Caskのインストール

Homebrewを拡張してアプリのパッケージ管理ができる[Homebrew-Cask](https://caskroom.github.io/)をインストールします。

```
$ brew tap caskroom/cask
```

### Gitのインストール

#### Mac OS

パッケージ管理システムである[Git](https://git-scm.com/)をインストールします。
パッケージからインストールする場合、以下のURLからインストーラを取得します。

[https://git-scm.com/download/mac](https://git-scm.com/download/mac)

または、Homebrewからインストールします。

```
$ brew install git
```

#### Windows

以下のURLから最新版のGitをインストールします。

[https://git-for-windows.github.io/](https://git-for-windows.github.io/)

### Node.jsのインストール

#### パッケージからのインストール

お使いのプラットフォームに応じたインストーラーやパッケージからインストールして下さい。

[https://nodejs.org/en/download/](https://nodejs.org/en/download/)

#### CLIからのインストール(推奨)

Mac OSの場合、Homebrewからもインストールできます。

```
$ brew install node
```

#### Node.jsの実行

インストールが完了したら、Nodeがインストールされているか確認して下さい。
以下のコマンドを、Mac OSの場合は`Terminal.app`から、Windowsの場合は`PowerShell.exe`から実行して下さい。
これ以降のコマンドも同様の環境で実行します。

```
$ node -v
v7.2.0

$ node
> 'Hello World'
'Hello World'
```

#### バージョンマネージャーからのインストール（オプション）

Nodeのバージョンスイッチャーである[nvm](https://github.com/creationix/nvm)を使用することができます。

インストールスクリプトが提供されています。

```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```

またはHomebrewからもインストールできます。

```
$ brew install nvm
```

Nodeのバージョン7をインストールします。

```
$ nvm install 7.3.1
```

#### yarnのインストール(オプション)

パッケージインストールの高速化と軽量化及びセキュリティ・信頼性の向上に役立つ`yarn`を使うことができます。

##### Mac OS

```
$ brew update
$ brew install yarn
```

##### Windows

パッケージからインストールができます。

https://yarnpkg.com/en/docs/install#windows-tab

### IDEのインストール（オプション）

#### Visual Studio Code(VSCode)

##### Mac OS

brew caskからインストールするか、

```
$ brew cask install visual-studio-code
```

パッケージからインストールができます。

https://code.visualstudio.com/

##### Windows

パッケージからインストールができます。

https://code.visualstudio.com/

### 本アプリのダウンロード

Gitコマンドを使ってReactサンプルをダウンロードします。

```
$ git clone https://kamataryo@bitbucket.org/ai-ot/ai-ot-seminar-01-hands-on.git
$ cd ai-ot-seminar-01-hands-on
$ ls
```

`ls`コマンドでプロジェクトのファイル・フォルダを確認してみて下さい。また、これ以降に紹介するコマンドはプロジェクトルートをカレントディレクトリとして実行します。

### 依存ライブラリのインストール

アプリはいくつかのライブラリ（開発ツール類も含む）に依存します。Node.jsのパッケージマネージャを使って、これらをインストールします。

```
$ npm install
```

npmの代わりにyarnを使うと、ライブラリの軽量化や高速なインストールを行うことができます。

```
$ yarn
# または
$ yarn install
```

Node 7系未満で一旦パッケージインストールをしてしまっている時は、依存ライブラリの再ビルドが必要です。以下のコマンドを使ってnode-sassをビルドしてください。

```
npm rebuild node-sass
```

nvmでNode.jsのバージョンを変える場合、以下のコマンドが使えます。リポジトリ内の`.nvmrc`を検出して自動でバージョンを判定します。

```
$ nvm install
```

### プロジェクトのテストの実行

以下のコマンドでプロジェクトのテストを実行できます。

```
$ npm test
```

### プロジェクトのビルドとプレビュー

以下のコマンドでプロジェクトをプレビューできます。

```
$ npm start
```

## 開発コマンド

|POSIX shell / CommandPrompt / PoswerShell|概要|
|:--|:--|:--|
|`npm install`または`yarn`|プロジェクトの依存ライブラリをインストールします。|
|`npm install xyz --save`または`yarn add xyz`||ライブラリ`xyz`をインストールします。|
|`npm install xyz --save-dev`または`yarn add xyz --dev`||開発環境への依存ライブラリを追加します。|
|`npm run migrate`|手動ブラウザチェッ用のユニットテストデータのインポートを行います。|
|`npm run compgen Xyz`||新しいコンポーネントXyzとテストを生成します。|
|`npm test`|テストを実行します。テストに失敗するとエラー(exit 1)になります。|
|`npm run lint`|JavaScriptの文法チェック(Lint)を行います。文法チェックに失敗するとエラー(exit 1)になります。|
|`npm run screenshot`|プロジェクトのスクリーンショットを更新します。|
|`npm start`|手動でのブラウザテスト用環境を立ち上げます。以下のサーバーが動作します。クライアントからのhttpアクセス（GET、PUT）を処理するモックWebサーバーなどが立ち上がり、また、ソースの変更の監視と自動ビルド、ブラウザの同期的再読み込みが行われます。モックWebサーバーに送られたデータはJSON形式のファイルとして、'./server/storage.json'にシリアライズされます。|
|`npm run build`|プロジェクトのプロダクション・コンパイルを行います。ブラウザがロードできる最適化されたスクリプトとスタイルシートを生成します。構文エラーなどでコンパイルが失敗すると、コマンドはエラー(exit 1)となります。|
|`npm run archive`|プロジェクトのプロダクションコードをアーカイブします。`./production.tar.gz`が得られます。|
