import update from 'immutability-helper'
import { getListIndex } from '../lib/search'
import Moment from 'moment'
/**
 * 時刻に関連する属性のアップデートを行います
 */
export default {

  /**
   * payloadのキーを指定する
   * @type {Array}
   */
  params: [
    'id',   // listID
    'type', // 'LEFT_AT' or 'START_AT'
    'value' // UNIX TIME serial, or Moment instance
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { id, type } = payload
    let value = payload.value

    if (value instanceof Moment) {
      value = value.unix() * 1000 + value.millisecond()
    }

    const { listIndex } = getListIndex(state, id)
    // 未発見時
    if (listIndex === -1) {
      return state
    }

    switch (type) {
      case 'LEFT_AT':
        return update(state, { lists: { [listIndex]: { leftAt: { $set: value } } } })
      case 'START_AT':
        return update(state, { lists: { [listIndex]: { startAt: { $set: value } } } })
      default:
        return state
    }
  }
}
