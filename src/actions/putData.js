import request from 'superagent'
import { state2JSON } from '../lib/stateAdaptor'

/**
 * サーバーにデータをputする
 */
export default {

  params: ['url'],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {
    request
      .put(payload.url)
      .set('Content-Type', 'application/json')
      .send({ data: state2JSON(state) })
      .end((err, res) => {
        if (JSON.parse(res.text).success) {
          /* eslint no-console: "off" */
          console.log(`PUT ${payload.url} success`)
        } else {
          /* eslint no-console: "off" */
          console.log(`PUT ${payload.url} fails`)
        }
      })

    return state
  }
}
