import { getInitialList } from '../constants'
import update from 'immutability-helper'

/**
 * リストを追加する操作
 */
export default {

  params: [
    'list'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    // 引数なし
    payload = payload || {}

    // listが定義されていない場合はデフォルトのものをクローン
    payload.list = payload.list || getInitialList()
    payload.list.postits = payload.list.postits || []

    // システム秒をIDとして指定します
    payload.list.id = Date.now()
    payload.list.id += ''

    return update(state, { lists: { $push: [payload.list] } })

  }
}
