import { state2JSON } from '../lib/stateAdaptor'

/**
 * コンソールに現在のstateを出力する
 */
export default {

  params: [],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: state => {
    // とりあえずコンソールに出す
    /* eslint no-console: "off" */
    console.log(state2JSON(state))
    return state
  }
}
