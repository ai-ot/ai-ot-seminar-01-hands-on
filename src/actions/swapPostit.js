import update from 'immutability-helper'
import { getPostitIndex } from '../lib/search'

/**
 * カードの並べ替え
 */
export default {

  params: [
    'idFrom',
    'idInto'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { idFrom, idInto } = payload

    if (idFrom === idInto) {
      return state
    }

    const from = getPostitIndex(state, idFrom)
    const into = getPostitIndex(state, idInto)

    // dock内交換
    if (from.isDocked && into.isDocked) {
      const postitFrom = state.dock.postits[from.postitIndex]
      const postitInto = state.dock.postits[into.postitIndex]

      return update(state, { dock: { postits: {
        [from.postitIndex]: { $set: postitInto },
        [into.postitIndex]: { $set: postitFrom }
      } } })

    // dock -> list
    } else if (from.isDocked) {
      const postitFrom = state.dock.postits[from.postitIndex]

      return update(state, {
        lists: { [into.listIndex]: { postits: { $splice: [[into.postitIndex, 0, postitFrom]] } } },
        dock: { postits: { $splice: [[from.postitIndex, 1]] } }
      })

    // list -> dock
    } else if (into.isDocked) {
      const postitFrom = state.lists[from.listIndex].postits[from.postitIndex]

      return update(state, {
        lists: { [from.listIndex]: { postits: { $splice: [[from.postitIndex, 1]] } } },
        dock: { postits: { $splice: [[into.postitIndex, 0, postitFrom]] } }
      })

    // list -> list
    } else {

      const postitFrom = state.lists[from.listIndex].postits[from.postitIndex]
      const postitInto = state.lists[into.listIndex].postits[into.postitIndex]

      if (from.listIndex === into.listIndex) {
        return update(state, { lists: { [from.listIndex]: { postits: {
          [from.postitIndex]: { $set: postitInto },
          [into.postitIndex]: { $set: postitFrom }
        } } } })
      } else {
        return update(state, { lists: {
          [from.listIndex]: { postits: { $splice: [[from.postitIndex, 1]] } },
          [into.listIndex]: { postits: { $splice: [[into.postitIndex, 0, postitFrom]] } }
        } })
      }
    }

  }
}
