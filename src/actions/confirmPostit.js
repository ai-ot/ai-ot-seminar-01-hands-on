import update from 'immutability-helper'
import { getPostitIndex } from '../lib/search'

/**
 * カードにconfirmプロパティを追加する
 */
export default {

  /**
   * payloadのキーを指定する
   * @type {Array}
   */
  params: [
    'id',
    'confirmed'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { id, confirmed } = payload
    const { listIndex, postitIndex, isDocked } = getPostitIndex(state, id)

    const value = confirmed ? true : false

    if (postitIndex === -1) {
      return state
    } else if (isDocked) {
      return update(state, { dock: { postits: { [postitIndex]: { confirmed: { $set: value } } } } })
    } else {
      return update(state, { lists: { [listIndex]: { postits: { [postitIndex]: { confirmed: { $set: value } } } } } })
    }
  }
}
