import update from 'immutability-helper'
import { getPostitIndex } from '../lib/search'
/**
 * カードにtoggle属性を付与、剥離する
 */
export default {

  params: [
    'id',
    'toggle'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {
    const { id, toggle } = payload

    const { listIndex, postitIndex, isDocked } = getPostitIndex(state, id)


    if (postitIndex === -1) {
      return state
    } else {
      if (isDocked) {
        return update(state, { dock: {
          postits: { [postitIndex]: { toggled: { $set: toggle } } },
          toggled: { $set: toggle }
        } })
      }
      return update(state, { lists: { [listIndex]: {
        postits: { [postitIndex]: { toggled: { $set: toggle } } },
        toggled: { $set: toggle }
      } } })
    }
  }
}
