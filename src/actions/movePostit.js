import update from 'immutability-helper'
import {
  getPostitIndex,
  getListIndex
} from '../lib/search'

/**
 * カードを別のリストに移動する操作
 */
export default {
  params: [
    'postitId',
    'listId'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { postitId, listId } = payload

    const { listIndex, postitIndex, isDocked } = getPostitIndex(state, postitId)

    const listSearchResult = getListIndex(state, listId)
    const destListIndex = listSearchResult.listIndex
    const destIsDock = listSearchResult.isDock

    // postit or destination not found
    if (postitIndex === -1 || (destListIndex === -1 && ! destIsDock )) {
      return state
    }

    let result

    let moving

    if (isDocked) {
      // dock -> list
      moving = state.dock.postits[postitIndex]
      result = update(state, {
        dock: { postits: { $splice: [[postitIndex, 1]] } },
        lists: { [destListIndex]: { postits: { $push: [moving] } } }
      })
    } else {
      moving = state.lists[listIndex].postits[postitIndex]
      if (destIsDock) {
        // list -> dock
        result = update(state, {
          lists: { [listIndex]: { postits: { $splice: [[postitIndex, 1]] } } },
          dock: { postits: { $push: [moving] } }
        } )
      } else {
        // list -> list
        result = update(state, { lists: {
          [listIndex]: { postits: { $splice: [[postitIndex, 1]] } },
          [destListIndex]: { postits: { $push: [moving] } }
        } })
      }
    }

    return result
  }

}
