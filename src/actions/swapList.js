import update from 'immutability-helper'
import { getListIndex } from '../lib/search'

/**
 * リストの並べ替え
 */
export default {

  params: [
    'idFrom',
    'idInto'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {
    const { idFrom, idInto } = payload
    if (idFrom === idInto) {
      return state
    }
    const indexFrom = getListIndex(state, idFrom).listIndex
    const indexInto = getListIndex(state, idInto).listIndex
    return update(state, { lists: {
      [indexFrom]: { $set: state.lists[indexInto] },
      [indexInto]: { $set: state.lists[indexFrom] }
    } })
  }
}
