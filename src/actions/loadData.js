import request from 'superagent'
import { JSON2state } from '../lib/stateAdaptor'

/**
 * リクエストを発出してデータサーバーから読み込む
 */
export default {

  params: [
    'url',
    'quiet',
    'callback'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {
    request
      .get(payload.url)
      .end((err, res) => {
        if (JSON.parse(res.text).success) {

          if (!payload.quiet) {
            /* eslint no-console: "off" */
            console.log(`GET ${payload.url} success`)
          }

          const { data } = JSON.parse(res.text)
          payload.callback(JSON2state(data))

        } else {
          if (!payload.quiet) {
            /* eslint no-console: "off" */
            console.log(`GET ${payload.url} fails`)
          }

          payload.callback({ title: 'データ読み込みエラー', lists: [] })
        }
      })
    // LOAD_DATAは読み込むだけでstateを更新しない
    return state
  }

}
