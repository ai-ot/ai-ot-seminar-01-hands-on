import update from 'immutability-helper'
import { getPostitIndex } from '../lib/search'
/**
 * カードの削除
 */
export default {

  params: [
    'id',
    'callback'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { id, callback } = payload

    const { listIndex, postitIndex, isDocked } = getPostitIndex(state, id)

    // postit not found
    if (postitIndex === -1) {
      return state
    }

    let result

    let deleting

    if (isDocked) {
      deleting = state.dock.postits[postitIndex]
      result = update(state, { dock: { postits: { $splice: [[postitIndex, 1]] } } })
    } else {
      deleting = state.lists[listIndex].postits[postitIndex]
      result = update(state, { lists: { [listIndex]: { postits: { $splice: [[postitIndex, 1]] } } } })
    }

    // コールバックを実行
    if (callback && typeof callback === 'function') {
      callback(deleting)
    }

    return result
  }
}
