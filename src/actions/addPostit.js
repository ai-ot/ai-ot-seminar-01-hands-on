import update from 'immutability-helper'
import { getInitialPostit } from '../constants'

/**
 * カードを追加する操作
 */
export default {

  /**
   * payloadのキーを指定する
   * @type {Array}
   */
  params: [
    'id',
    'postit'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { id } = payload
    let { postit } = payload

    // postitが定義されていない場合はデフォルトのものをクローンします
    postit = postit || getInitialPostit()

    // 現在ではシステム秒をIDにしている
    postit.id = '' + (postit.id || Date.now())

    // dockに追加するケース
    if (state.dock && state.dock.id === id) {
      if (! state.dock.postits) {
        return update(state, { dock: { postits: { $set: [postit] } } } )
      } else {
        return update(state, { dock: { postits: { $push: [postit] } } })
      }
    } else {

      // カードを追加するリストのインデックスを取得
      let listIndex = -1
      for (var i = 0; i < state.lists.length; i++) {
        if (state.lists[i].id === id) {
          listIndex = i
          break
        }
      }

      // インデックスが見つからなければそのままstateを返す
      if (listIndex === -1) {
        return state
      } else {
        return update(state, { lists: { [listIndex]: { postits: { $push: [postit] } } } })
      }
    }
  }
}
