import update from 'immutability-helper'
import { getListIndex } from '../lib/search'

/**
 * リストの削除
 */
export default {

  params: [
    'id'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const { listIndex } = getListIndex(state, payload.id)

    if (listIndex === -1) {
      return state
    } else {
      return update(state, { lists: { $splice: [[listIndex, 1]] } })
    }
  }
}
