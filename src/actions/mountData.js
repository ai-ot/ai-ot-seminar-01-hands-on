/**
 * 渡されたstateをマウントするだけのアクション
 * コールバックに渡して使う
 */
export default {

  params: [
    'state'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => payload.state
}
