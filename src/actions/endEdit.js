import { wrapValueWithEditting } from '../lib/stateAdaptor'
import update from 'immutability-helper'
import {
  getListIndex,
  getPostitIndex
} from '../lib/search'

/**
 * typeとidで与えられる編集可能要素の編集可能状態を終了する
 */
export default {

  params: [
    'type',
    'id',
    'value'
  ],

  /**
   * 関数本体
   * @param  {State} state    Immutableなstateです。このオブジェクトを変更すべきではありません。
   * @param  {Object} payload 関数に与える引数です。
   * @return {State}          適切にUpdateされた新しいStateを返却します。
   */
  func: (state, payload) => {

    const value =  wrapValueWithEditting(payload.value, false)
    const { type, id } = payload

    switch (type) {

      case 'BOARD_TITLE': {
        return update(state, { title: { $set: value } })
      }

      case 'LIST_TITLE': {
        const { listIndex } = getListIndex(state, id)
        if (listIndex === -1) {
          return state
        } else {
          return update(state, { lists: { [listIndex]: { title: { $set: value } } } })
        }
      }

      case 'CARD_TITLE': {
        const { listIndex, postitIndex, isDocked } = getPostitIndex(state, id)
        if (postitIndex === -1) {
          return state
        } else {
          if (isDocked) {
            return update(state, { dock: { postits: { [postitIndex]: { title: { $set: value } } } } })
          } else {
            return update(state, { lists: { [listIndex]: { postits: { [postitIndex]: { title: { $set: value } } } } } })
          }
        }
      }

      case 'CARD_CONTENT': {
        const { listIndex, postitIndex, isDocked } = getPostitIndex(state, id)
        if (postitIndex === -1) {
          return state
        } else {
          if (isDocked) {
            return update(state, { dock: { postits: { [postitIndex]: { content: { $set: value } } } } })
          } else {
            return update(state, { lists: { [listIndex]: { postits: { [postitIndex]: { content: { $set: value } } } } } })
          }
        }
      }

      default: {
        return state
      }
    } // end switch
  }

}
