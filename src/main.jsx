import React                 from 'react'
import ReactDom              from 'react-dom'
import { createStore }       from 'redux'
import { connect, Provider } from 'react-redux'
import {
  mapState2Props,
  mapDispatch2Props,
  reducer
}                            from './redux'
// import { getInitialBoard }   from './constants'
import { JSON2state } from './lib/stateAdaptor'
import _Board from './components/Board.jsx'

/**
 * エントリポイントになるBoardコンポーネントをRedux関連のモジュールでデコレートする
 * @type {Component}
 */
const Board = connect(mapState2Props, mapDispatch2Props)(_Board)

/**
 * とりあえずの初期データ
 * @type {Object}
 */
const data = {
  'title': '新しいボード',
  'lists': [
    {
      'id': 'list-001',
      'title': 'カードが無い、空のリスト',
      'postits': []
    }
    ,
    {
      'id': 'list-002',
      'title': 'カードが2枚入ったリスト',
      'postits': [
        {
          'id': 'postit-1',
          'title': 'カード1',
          'content': 'カードの内容'
        },
        {
          'id': 'postit-2',
          'title': 'カード2',
          'content': 'カードの内容'
        }
      ]
    }
  ],
  'dock': {
    'id': 'the-dock-',
    'title': '未配置のカードを格納する要素です',
    'postits': [
      {
        'id': 'dock-postit-1',
        'title': '未配置カード1',
        'content': '未配置のカードの内容'
      }
    ]
  }
}

/**
 * Reactのレンダリングのエントリーポイント
 * @type {Object}
 */
ReactDom.render(
  <Provider
    store={ createStore(reducer, JSON2state(data)) } >
    <Board />
  </Provider>,
  document.getElementById('main')
)
