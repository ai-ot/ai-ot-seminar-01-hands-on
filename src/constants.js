/**
 * 定数定義用モジュール
 */

import { wrapValueWithEditting } from './lib/stateAdaptor'

/**
 * 初期状態のBoardを作成します。
 * Boardはstateの最上位の構成要素で、stateと同じ構造になります。
 * @return {object} [description]
 */
export const getInitialBoard = () => {
  return {
    title: wrapValueWithEditting('新しいボード'),
    dock: { id: 'the-dock' },
    lists: []
  }
}

/**
 * 初期状態のListを作成します。
 * @return {Object} [description]
 */
export const getInitialList = () => {
  return {
    title: wrapValueWithEditting('新しいリスト'),
    postits: []
  }
}

/**
 * 初期状態のカードを作成します。
 * @return {Object} [description]
 */
export const getInitialPostit = () => {
  return {
    title: wrapValueWithEditting('新しいカード'),
    content: wrapValueWithEditting('新しいコンテンツ')
  }
}
