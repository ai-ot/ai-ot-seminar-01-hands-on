/**
 * [deprecated] この関数はおそらく近いうちに廃止します。
 * Immutability-helperでの置き換えを検討しています。
 * 所定の型のオブジェクトを走査して反復的に処理を実施します
 * @param  {Object}   obj [対象オブジェクト]
 * @param  {Function} app [反復処理]
 * @return {Object}       反復処理が適用されたオブジェクト
 */
const scan = (obj, app) => {

  return {
    title: app(obj.title),
    lists: (obj.lists || []).map(list => {
      return {
        leftAt: list.leftAt,
        startAt: list.startAt,
        id: list.id,
        title: app(list.title),
        postits: (list.postits || []).map(postit => {
          return {
            id: postit.id,
            title: app(postit.title),
            content: app(postit.content)
          }
        })
      }
    }),
    dock: {
      id: obj.dock ? obj.dock.id : undefined,
      title: app(obj.dock ? obj.dock.title : undefined),
      postits: (obj.dock ? (obj.dock.postits || []) : []).map(postit => {
        return {
          id: postit.id,
          title: app(postit.title),
          content: app(postit.content)
        }
      })
    }
  }
}

/**
* 値を所定の型のオブジェクトで包みます
 * @param  {Object}  value            [description]
 * @param  {Boolean} [editting=false] [description]
 * @return {Object}                   [description]
 */
export const wrapValueWithEditting = (value, editting = false) => {
  return { editting, value }
}

/**
 * 所定の形のオブジェクトを剥きます。obj.valueの糖衣構文です。
 * @param  {Object} obj [description]
 * @return {Object}     [description]
 */
export const unwrapValueWithEditting = obj => obj.value

/**
 * 読み込んだJSONを整形してstateに変換します
 * @param  {Object} json [description]
 * @return {Object}      [description]
 */
export const JSON2state = json => scan(json, wrapValueWithEditting)

/**
 * stateを整形して保存できるJSONに変換します
 * @param  {Object} state [description]
 * @return {Object}       [description]
 */
export const state2JSON = state => scan(state, unwrapValueWithEditting)
