/**
 * stateを検索する操作
 */

/**
 * リストのIDからそのIndex値を検索します
 * @param  {State} state  [description]
 * @param  {String} listId [description]
 * @return {Object}        [description]
 */
export const getListIndex = (state, listId) => {

  // dockのことを探そうとしているかどうかを検証
  if (
    state.dock &&
    state.dock.id &&
    state.dock.id === listId ) {
    return { listIndex: -1, isDock: true }
  }

  // listsを検索
  for (var i = 0; i < (state.lists || []).length; i++) {
    let list = state.lists[i]
    if (list.id === listId) {
      return { listIndex: i, isDock: false }
    }
  }

  // 見つからない時
  return { listIndex: -1, isDock: false }
}

/**
 * カードのIDからそのインデックス値を検索します
 * @param  {State} state  [description]
 * @param  {String} postitId [description]
 * @return {Object}        {postitIndex, listIndex, isDocked}
 */
export const getPostitIndex = (state, postitId) => {

  // dock内部を検索
  for (var h = 0; h < (state.dock ? (state.dock.postits || []) : []).length; h++) {
    let postit = state.dock.postits[h]
    if (postit.id === postitId) {
      return {
        listIndex: -1,
        postitIndex: h,
        isDocked: true
      }
    }
  }

  // lists内部を検索
  for (var i = 0; i < (state.lists ? state.lists : []).length; i++) {
    let list = state.lists[i]
    for (var j = 0; j < (list.postits ? list.postits : []).length; j++) {
      let postit = list.postits[j]
      if (postit.id === postitId) {
        return {
          listIndex: i,
          postitIndex: j,
          isDocked: false
        }
      }
    }
  }

  // 見つからない時
  return {
    listIndex: -1,
    postitIndex: -1,
    isDocked: false
  }
}
