/**
 * 定数
 */
import { getInitialBoard } from './constants'

/**
 * actionの実装をロード
 */
import loadData      from './actions/loadData'
import putData       from './actions/putData'
import mountData     from './actions/mountData'
import serializeData from './actions/serializeData'
import addList       from './actions/addList'
import deleteList    from './actions/deleteList'
import addPostit       from './actions/addPostit'
import deletePostit    from './actions/deletePostit'
import swapList      from './actions/swapList'
import swapPostit      from './actions/swapPostit'
import startEdit     from './actions/startEdit'
import endEdit       from './actions/endEdit'
import movePostit      from './actions/movePostit'
import togglePostit    from './actions/togglePostit'
import confirmPostit   from './actions/confirmPostit'
import updateTime    from './actions/updateTime'
/**
 * kvsにアクションを格納する。アクション一覧をテストで使うため、exportしていますが、
 * このオブジェクトを直接使うべきではありません。
 * @type {Object}
 */
export const _allActions = {
  loadData,
  putData,
  mountData,
  serializeData,
  addList,
  addPostit,
  deleteList,
  deletePostit,
  swapList,
  swapPostit,
  startEdit,
  endEdit,
  movePostit,
  togglePostit,
  confirmPostit,
  updateTime
}

/**
 * actionをstoreへdispatchするマッピング処理
 * actionの引数をpayloadオブジェクトとしてラップすることで型を決めている
 * @param  {Function} dispatch [description]
 * @param  {[type]} props    [description]
 * @return {Object}          マッピングされたactionsa
 */
export const mapDispatch2Props = (dispatch, props) => {

  const actionHandlers = {}

  Object.keys(_allActions).forEach(type => {

    actionHandlers[type] = (...args) => e => {

      // payloadオブジェクトに引数を変換
      const payload = {}
      _allActions[type].params.forEach((dependency, index) => {
        if (args.length >= index) {
          payload[dependency] = args[index]
        }
      })
      if (type === 'endEdit') { payload.value = e.target.value }
      dispatch( { type, payload } )
    }
  })

  return { actionHandlers }
}

/**
 * [mapState2Props description]
 * @param  {Object} state [description]
 * @param  {[type]} props [description]
 * @return {Object}       state
 */
export const mapState2Props = (state, props) => state

/**
 * stateとactionから新しいstateを返す
 * @param  {[type]} [state=initialState] [description]
 * @param  {Object} action               [Function] funcを持つオブジェクト
 * @return {[type]}                      [description]
 */
export const reducer = (state = getInitialBoard(), action) => {
  return (action.type in _allActions) ?
    _allActions[action.type].func(state, action.payload) :
    state
}
