import React               from 'react'
import { DragDropContext } from 'react-dnd'
import HTML5Backend        from 'react-dnd-html5-backend'
import TouchBackend        from 'react-dnd-touch-backend'

import List   from './List.jsx'
import Header from './Header.jsx'
import Dock   from './Dock.jsx'

/**
 * React-DnD用
 * @type {RegExp}
 */
const mobilePattern = /(Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone)/i

/**
 * モバイル判定をクライアント側で行う
 * @type {Object}
 */
const backend = (
  navigator
    .userAgent
    .match(mobilePattern) === null
) ? HTML5Backend : TouchBackend

@DragDropContext(backend)
/**
 * カードボードの既定になるクラス。
 * Reactのコンポーネントツリーのエントリポイント。
 * @type {Component}
 */
class Board extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {
    const { title, lists, actionHandlers } = this.props
    const { dock } = this.props

    return <div className='board'>

      <Header
        id={ 'the-header' }
        title={ title }
        actionHandlers={ actionHandlers }
        />

      <ul className='ul'>

        { (
          /**
           * // dockプロパティが定義されている時のみrenderするようにします。
           * @return {ReactDOM} [ReactDOM]
           */
          () => {
            if (dock) {
              return <Dock
                key={ dock.id }
                id={ dock.id }
                title={ dock.title }
                postits={ dock.postits }
                actionHandlers={ actionHandlers } />
            }
          }
        )() }

        {
          /**
           * リスト要素を列挙します。
           * @return {ReactDOM} [ReactDOM]
           */
          (lists || []).map(({
            id,
            title,
            toggled,
            postits,
            leftAt,
            startAt
          }) => <List
            key={ id }
            id={ id }
            title={ title }
            toggled={ toggled }
            postits={ postits }
            leftAt={ leftAt }
            startAt={ startAt }
            actionHandlers={ actionHandlers } />
          )
        }
      </ul>

    </div>
  }
}

/**
 * validation
 * @type {Object}
 */
Board.propTypes = {
  title:          React.PropTypes.object,
  lists:          React.PropTypes.array,
  dock:           React.PropTypes.object,
  actionHandlers: React.PropTypes.object
}

/**
 * module exportation
 */
export default Board
