import React from 'react'

/**
 * 編集可能テキストエリアのコンポーネント
 * @type {Component}
 */
class TextAreaEditable extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {

    const { text, type, id, actionHandlers } = this.props
    const value = (text.value == '') ? '( No text )' : text.value

    if (text.editting) {

      return <form>
        <textarea
          className='input-textarea'
          defaultValue={ text.value }
          autoFocus
          onBlur={ actionHandlers.endEdit(type, id) }
          onKeyDown={ e => {
            if (e.keyCode === 13 && ! e.shiftKey) {
              actionHandlers.endEdit(type, id)(e)
            }
          } }
        />
    </form>

    } else {

      return <div>
        <span
          className='click-to-input'
          onClick={ actionHandlers.startEdit(type, id, text.value) }
        >{ value }</span>
      </div>
    }
  }

}

/**
 * Validation
 * @type {Object}
 */
TextAreaEditable.propTypes = {
  text:           React.PropTypes.object,
  type:           React.PropTypes.string,
  id:             React.PropTypes.string,
  actionHandlers: React.PropTypes.object
}

export default TextAreaEditable
