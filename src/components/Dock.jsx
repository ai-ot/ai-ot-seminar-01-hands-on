import { DropTarget } from 'react-dnd'
import React from 'react'
import Postit from './Postit.jsx'
import PostitEmpty from './PostitEmpty.jsx'

 @DropTarget(
   'list',
   {
     hover: (props, monitor, component) => {

       const droppedId = props.id
       const draggedId = monitor.getItem().id
       props.actionHandlers.movePostit(draggedId, droppedId)()
     }
   },
   (connect, monitor) => {
     return {
       connectDropTarget: connect.dropTarget(),
       canDrop: monitor.canDrop(),
       isOver: monitor.isOver({ shallow: false })
     }
   }
 )
 /**
  * 未配置のカード置き場コンポーンント
  * @type {Component}
  */
class Dock extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {

    const { id, postits, title, actionHandlers } = this.props

    const validatedTitle = title ?
      title.value !== '' ?
        title.value :
        '(NO TITLE)'
      : '(NO TITLE)'
    return <li className="dock list clearfix">
      <h2 className="title">{ validatedTitle }</h2>
        <ul className='ul'>

          { (
            /**
             * postitがあれば列挙
             * @return {ReactDOM} [description]
             */
            () => {
              if (postits && postits.length > 0) {
                return (postits || []).map(({ id, title, toggled, confirmed, content }) =>
                  <Postit
                    key={ id }
                    id={ id }
                    title={ title }
                    toggled={ toggled }
                    confirmed={ confirmed ? true : false }
                    content={ content }
                    actionHandlers={ actionHandlers } />
                )
              } else {
                return <PostitEmpty
                  id={ id + '-postit-drop-target' }
                  listId={ id }
                  actionHandlers={actionHandlers} />
              }
            }
          )() }

        </ul>

        <button
          className='button button-add'
          onClick={ actionHandlers.addPostit(id) }
        >カードを追加</button>
    </li>
  }
}

Dock.propTypes = {
  id:             React.PropTypes.string.isRequired,
  title:          React.PropTypes.object,
  postits:          React.PropTypes.array,
  actionHandlers: React.PropTypes.object
}

/**
 * module exportation
 */
export default Dock
