import React        from 'react'
import classNames   from 'classnames'
import TimePicker   from 'rc-time-picker'
import Moment       from 'moment'
import Postit         from './Postit.jsx'
import PostitEmpty    from './PostitEmpty.jsx'
import TextEditable from './TextEditable.jsx'

/**
 * 複数のカードを内包するListコンポーネント
 * @type {Component}
 */
class List extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {
    const { id, postits, actionHandlers } = this.props
    const title = this.props.title || {}
    const leftAt  = this.props.leftAt  || 0
    const startAt = this.props.startAt || 0

    const classes = classNames({
      list: true,
      clearfix: true,
      'has-confirmed': ((postits || []).map(postit => postit.confirmed).indexOf(true) !== -1),
      'has-toggled': ((postits || []).map(postit => postit.toggled).indexOf(true) !== -1)
    })

    return <li key={ id }  className={ classes }>
      <h2
        className='list-title'
        tabIndex='0'
        onFocus={ actionHandlers.startEdit('LIST_TITLE', id, title) }
      >
        <TextEditable
          text={ title }
          type={ 'LIST_TITLE' }
          id={ id }
          actionHandlers={ actionHandlers }
        />
      </h2>

      <ul className='ul'>
        { (
          /**
           * Postitを列挙します。なければ、空カードを返します。
           * @return {ReactDOM} [description]
           */
          () => {
            if (postits && postits.length > 0) {
              return (postits || []).map(({ id, title, toggled, confirmed, content }) =>
                <Postit
                  key={ id }
                  id={ id }
                  title={ title }
                  toggled={ toggled }
                  confirmed={ confirmed ? true : false }
                  content={ content }
                  actionHandlers={ actionHandlers } />
              )
            } else {
              return <PostitEmpty
                id={ id + '-postit-drop-target' }
                listId={ id }
                actionHandlers={actionHandlers} />
            }
          }
        )() }
      </ul>

      <button
        className='button button-add'
        onClick={ actionHandlers.addPostit(id) }
      >カードを追加</button>

      <button
        className='button button-delete'
        onClick={ actionHandlers.deleteList(id) }
        >
        { 'リストを削除' }
      </button>
    </li>
  }
}

/**
 * Validation
 * @type {Object}
 */
List.propTypes = {
  id:             React.PropTypes.string.isRequired,
  leftAt:         React.PropTypes.number,
  startAt:        React.PropTypes.number,
  title:          React.PropTypes.object,
  postits:          React.PropTypes.array,
  actionHandlers: React.PropTypes.object.isRequired,

  connectDragSource: React.PropTypes.func,
  connectDropTarget: React.PropTypes.func
}

/**
* module exportation
 */
export default List
