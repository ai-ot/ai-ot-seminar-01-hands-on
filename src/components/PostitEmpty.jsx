import React          from 'react'
import { DropTarget } from 'react-dnd'
import classNames     from 'classnames'

@DropTarget(
  'postit',
  {
    hover: (props, monitor, component) => {
      const droppedId = props.listId
      const draggedId = monitor.getItem().id
      props.actionHandlers.movePostit(draggedId, droppedId)()
    }
  },
  (connect, monitor) => {
    return {
      connectDropTarget: connect.dropTarget(),
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver({ shallow: true })
    }
  }
)
/**
 * List内にカードが存在しないときにレンダリングされる要素
 * 空のリストのDrag and Dropのターゲットなる。
 * @type {Compnent}
 */
class PostitEmpty extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {
    const { id } = this.props
    // React-DnD module
    const { connectDropTarget } = this.props
    // React-DnD result
    const { isDragging, canDrop, isOver } = this.props

    const classes = classNames({
      postit: true,
      'drop-target': true,
      'postit-empty': true
    })

    return connectDropTarget(
      <li
        key={ id }
        className={ classes }>
        <h3
          className='empty-postit-title'
        >{ 'ここにPostitをDrop' }
        </h3>
      </li>
    )
  }
}

/**
 * Validation
 * @type {Object}
 */
PostitEmpty.propTypes = {
  id:             React.PropTypes.string.isRequired,
  listId:         React.PropTypes.string.isRequired,
  actionHandlers: React.PropTypes.object.isRequired,
  isDragging:     React.PropTypes.bool,
  canDrop:        React.PropTypes.bool,
  isOver:         React.PropTypes.bool,

  connectDropTarget: React.PropTypes.func
}

/**
 * module exportation
 */
export default PostitEmpty
