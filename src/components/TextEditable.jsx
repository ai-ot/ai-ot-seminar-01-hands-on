import React from 'react'

/**
 * 編集可能テキストのコンポーネント
 * @type {Component}
 */
class TextEditable extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {

    const { text, type, id, actionHandlers } = this.props
    const value = text.value ? text.value : '( No text )'

    if (text.editting) {

      return <form>
        <input
          className='input-text'
          defaultValue={ text.value }
          autoFocus
          onBlur={ actionHandlers.endEdit(type, id) }
          onKeyDown={ e => {
            if (e.keyCode === 13) {
              actionHandlers.endEdit(type, id)(e)
            }
          } }
        />
      </form>

    } else {

      return <div>
        <span
          className='click-to-input'
          onClick={ actionHandlers.startEdit(type, id, text.value) }
        >{ value }</span>
      </div>
    }
  }

}
/**
 * Validation
 * @type {Object}
 */
TextEditable.propTypes = {
  text:           React.PropTypes.object.isRequired,
  type:           React.PropTypes.string,
  id:             React.PropTypes.string,
  actionHandlers: React.PropTypes.object
}

export default TextEditable
