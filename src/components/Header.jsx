import React from 'react'
import TextEditable from './TextEditable.jsx'

/**
 * [Component description]
 * @type {Component}
 */
class Header extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {

    // the props
    const {
        id,
        title,
        actionHandlers
    } = this.props

    return <div
      key={ id }
      className='header'>
      <h1
        className='title'
        tabIndex='0'
        onFocus={ actionHandlers.startEdit(
          'BOARD_TITLE',
          undefined,
          title.value
        ) }
      >
        <TextEditable
          text={ title }
          type={ 'BOARD_TITLE' }
          actionHandlers={ actionHandlers }
        />
        </h1>

      <button
        id='add-list'
        className='button button-add'
        onClick={ actionHandlers.addList() }>
        { '新しいリストを追加' }
      </button>

      <button
        className='button button-add'
        onClick={ actionHandlers.loadData(
          'http://localhost:2999/storage.json',
          false, // quietオプション
          state => actionHandlers.mountData(state)()
        ) }
      >データ取得</button>

      <button
        className='button button-add'
        onClick={ actionHandlers.putData(
          'http://localhost:2999/storage.json'
        ) }
      >データ保存</button>

      <button
        className='button button-add'
        onClick={ actionHandlers.serializeData() }
      >データをコンソールに出力</button>

    </div>
  }

}

/**
 * Validation
 * @type {Object}
 */
Header.propTypes = {
  id:             React.PropTypes.string.isRequired,
  title:          React.PropTypes.object,
  actionHandlers: React.PropTypes.object,
}

/**
 * module exportation
 */
export default Header
