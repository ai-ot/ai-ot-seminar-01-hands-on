import React                      from 'react'
import { DragSource, DropTarget } from 'react-dnd'
import classNames                 from 'classnames'

import TextEditable     from './TextEditable.jsx'
import TextAreaEditable from './TextAreaEditable.jsx'

@DragSource(
  'postit',
  {
    beginDrag: ( props, monitor, component ) => {
      return { id: props.id }
    }
  },
  (connect, monitor) => {
    return {
    // Call this function inside render()
    // to let React DnD handle the drag events:
      connectDragSource: connect.dragSource(),
    // You can ask the monitor about the current drag state:
      isDragging: monitor.isDragging(),
    }
  }
)
@DropTarget(
  'postit',
  {
    hover: (props, monitor, component) => {
      const droppedId = props.id
      const draggedId = monitor.getItem().id
      props.actionHandlers.swapPostit(draggedId, droppedId)()
    }
  },
  (connect, monitor) => {
    return {
      connectDropTarget: connect.dropTarget(),
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver({ shallow: true })
    }
  }
)
/**
 * Listに内包されるカード要素。カードボード上の情報の最小単位
 * @type {Compnent}
 */
class Postit extends React.Component {

  /**
   * constructor
   * @return {ReactDOM} [ReactDOM]
   */
  render() {
    const { id, title, content, actionHandlers } = this.props
    // toggle, confirm
    const { toggled, confirmed } = this.props
    // React-DnD module
    const { connectDragSource, connectDropTarget } = this.props
    // React-DnD result
    const { isDragging, canDrop, isOver } = this.props

    const classes = classNames({
      postit: true,
      toggled:   toggled,
      confirmed: confirmed ? true : false,
      dragging:  toggled ? false : isDragging,
      droppable: toggled ? false : canDrop,
      dropping:  toggled ? false : isOver
    })

    const postitDOM = <li className={ toggled ? 'toggled-wrapper' : '' }>
      <div
        key={ id }
        className={ classes }>
        <h3
          className='title'
          tabIndex='0'
          onFocus={ toggled ?
            actionHandlers.startEdit('CARD_TITLE', id, title.value) :
            () => {}
          }
        >
          <TextEditable
            text={ title }
            type={ 'CARD_TITLE' }
            id={ id }
            actionHandlers={ actionHandlers }
          />
        </h3>
        <div
          className='content'
          tabIndex='0'
          onFocus={ toggled ?
            actionHandlers.startEdit('CARD_CONTENT', id, content.value) :
            () => {} // toggleされている時に押しても何も起きないように空関数を渡している
          }
        >{ content.value }
        </div>

          <button
            className='button button-delete'
            onClick={ actionHandlers.deletePostit(id) }
          >
            { 'カードを削除' }
          </button>

      </div>
    </li>

    if (toggled || confirmed) {
      // toggle状態とは、モーダルウィンドウを表示した時のこと
      // 非toggleは特にcollapseと呼ぶこともある
      // toggle状態の時はDnDコンテキストを返さない
      return postitDOM
    } else {
      // collapse状態の時はDnDコンテキストを返す
      return connectDropTarget(connectDragSource(postitDOM))
    }
  }
}

/**
 * Validation
 * @type {Object}
 */
Postit.propTypes = {
  id:             React.PropTypes.string.isRequired,
  title:          React.PropTypes.object,
  content:        React.PropTypes.object,
  actionHandlers: React.PropTypes.object.isRequired,

  toggled:        React.PropTypes.bool,
  confirmed:      React.PropTypes.bool,

  isDragging:     React.PropTypes.bool,
  canDrop:        React.PropTypes.bool,
  isOver:         React.PropTypes.bool,

  connectDragSource: React.PropTypes.func,
  connectDropTarget: React.PropTypes.func
}

/**
 * module exportation
 */
export default Postit
