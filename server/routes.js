/**
 * Mockサーバー用のルーティングファイルです。
 */

import { readFile, writeFile } from 'fs'
import { blue }                from 'colors/safe'


export default conf => {

  /**
   * do log
   * @param  {String} text Log text
   * @return {void}
   */
  const log = text => {
    if (!conf.quiet) {
      process.stdout.write(`[${blue('Mock Server')}] ${text}\n`)
    }
  }

  /**
   * GETするルート
   * @param  {Object} req [description]
   * @param  {Object} res [description]
   * @return {void}
   */
  const getAll = (req, res) => {

    log('data requested')

    readFile('./server/storage.json', 'utf8', (err, data) => {
      if (err) {
        // Storageが見つからなかった時はデフォルトのseeds.jsonを返す
        readFile('./server/seeds.json', 'utf8', (err, data) => {
          if (err) {
            // seedsも見つからなかった時は失敗
            log('default data not found')

            const message = 'unknown error. default  mock data not found'
            res.send({ success: false, data: { message } })
          } else {

            log('default data found')

            res.send({ success: true, data: JSON.parse(data) })
          }
        })
      } else {
        // Storageが見つかった時はそれを返す
        log('storage data found')

        res.send({ success: true, data: JSON.parse(data) })
      }
    })

  }

  /**
   * PUTするルート
   * @param  {Object} req [description]
   * @param  {Object} res [description]
   * @return {void}
   */
  const putAll = (req, res) => {

    writeFile('./server/storage.json', JSON.stringify(req.body.data), err => {
      if (err) {
        log('data put failed')
        res.send({ success: false })
      } else {
        log('data put success')
        res.send( { success: true })
      }
    })

  }

  return { getAll, putAll }
}
