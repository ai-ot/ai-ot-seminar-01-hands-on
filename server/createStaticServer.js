import express    from 'express'
import { yellow } from 'colors/safe'

/**
 * 静的ファイルサーバーを起動するPromiseを返します
 * @param  {Number} port [サーバーのポート番号]
 * @return {Promise}      [description]
 */
export default port => new Promise((resolved, rejected) => {

  express()
  // サーバー起動
    .use(express.static('./'))
    .listen(port, () => {
      process.stdout.write(`[${yellow('static Server')}] server started\n`)
      resolved()
    })
    .on('error', err => {
      rejected(err)
    })

})
