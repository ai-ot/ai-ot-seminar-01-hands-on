import express    from 'express'
import corser     from 'corser'
import bodyParser from 'body-parser'
import { blue }   from 'colors/safe'
import router     from './routes.js'

/**
 * サーバー起動用デフォルトのオプション
 * @type {Object}
 */
const confDefault = {
  port: 2999,
  quiet: false
}

/**
 * モックサーバーを立てるプロミスを返します
 * @param  {Object} conf { port: {Number} port番号, quiet: {Bool} 標準出力するかどうか }
 * @return {Promise}      [description]
 */
export default conf => new Promise((resolved, rejected) => {

  conf = {
    port: conf.port !== undefined ? conf.port : confDefault.port,
    quiet: conf.quiet !== undefined ? conf.quiet : confDefault.quiet
  }

  express()
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    // ebnable CORS
    .use(corser.create({
      methods: [
        'GET',
        'PUT'
      ]
    }))
    // データを全部返す
    .get('/storage.json', router(conf).getAll)
    // データを全部更新する
    .put('/storage.json', router(conf).putAll)
    // サーバー起動
    .listen(conf.port, () => {
      if (!conf.quiet) {
        process.stdout.write(`[${blue('mock Server')}] server started\n`)
      }
      resolved()
    })
    .on('error', err => {
      rejected(err)
    })
})
